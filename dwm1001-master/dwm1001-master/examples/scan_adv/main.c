/* main.c - Application main entry point */

/*
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <sys/printk.h>
#include <sys/util.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>

static u8_t mfg_data[] = { 0xff, 0xff, 0x00, 0x09 };
u8_t scanstruct = 0x00;


static const struct bt_data ad[] = {
	/*BT_DATA(BT_DATA_MANUFACTURER_DATA, mfg_data, 3),*/
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),

  BT_DATA(BT_DATA_SVC_DATA32, mfg_data, 4)
};


//Funzione Ricezione Advertisement
static void scan_cb(const bt_addr_le_t *addr, s8_t rssi, u8_t adv_type,
		    struct net_buf_simple *buf)
{
	//rssi
	printk("rssi %d\n",rssi);
	//type
	printk("type %d\n",adv_type);
	//indirizzo
	printk("addr");
	printk("%x ",addr->a.val[5]);
	printk("%x ",addr->a.val[4]);
	printk("%x ",addr->a.val[3]);
	printk("%x ",addr->a.val[2]);
	printk("%x ",addr->a.val[1]);
	printk("%x ",addr->a.val[0]);
	printk("\nbuffer\n");

	//DATA from advertisement
	u16_t lunghezzaARRAYbuffer = buf->len;
	unsigned char arraybuffer[lunghezzaARRAYbuffer];
	for(int i=0; i<lunghezzaARRAYbuffer; i++){
		arraybuffer[i]=net_buf_simple_pull_u8(&(buf->data));
		printk("%x ",arraybuffer[i]);
	}
	printk("\n");

	//numero totale di advertisement rilevati
	scanstruct;
}

/*funzione che genera due numeri HEX casuali e li mette nel pacchetto di asverti
sement prodotto dalla scheda DWM1001*/
uint8_t valore1[]={0x01};
uint8_t valore2[]={0x02};
void updatepackble(){
	//printk("primoelemento %d", mfg_data[1]);
	sys_rand_get(valore1, sizeof(valore1[0]));
	//printk("primocasuale %d", valore1[0]);
	sys_rand_get(valore2, sizeof(valore2[0]));
	mfg_data[0]=valore1[0];
	mfg_data[1]=valore2[0];
}

uint16_t myID;
uint16_t newid=0;
uint16_t zero=0;


void getMyID(uint16_t *tagID)
{
  	//*tagID = (uint16_t) sys_rand32_get()%65535; //65535 is reserved for broadcast.
		*tagID = (uint16_t) zero;
}


void dw_main(void)
{


  //definizione parametri per fare lo scan degli advertisement
	struct bt_le_scan_param scan_param = {
		.type       = BT_HCI_LE_SCAN_PASSIVE,
		.filter_dup = BT_HCI_LE_SCAN_FILTER_DUP_DISABLE,
		.interval   = 0x0010,
		.window     = 0x0010,
	};
	int err;
	int err1;
	int err2;
    int scan_ads=0;
    int print_commands=1;

	printk("Starting Scanner/Advertiser Demo\n");

	/* Initialize the Bluetooth Subsystem */
	/*err = bt_enable(NULL);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	printk("Bluetooth initialized\n");*/


  //funzione che fa partire lo scan dei pacchetti di advertisement
	if (scan_ads)
    {
        err = bt_le_scan_start(&scan_param, scan_cb);
        if (err) {
            printk("Starting scanning failed (err %d)\n", err);
		return;
        }
    }
  /*spegnere advertising che parte in automatico
	ble_stop_advertising();*/

	//funzione da far partire solamente dopo aver stoppato advertisement;
	//riavvia l'advertisement con parametri che possono essere liberamente impostati
	//nella funzione seguente
	/*err = bt_le_adv_start(BT_LE_ADV_NCONN, ad, ARRAY_SIZE(ad),
						NULL, 0);
	printk("advstart %d",err);
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}*/
    if(print_commands)
    {
        getMyID(&myID);
        do {
		k_sleep(K_MSEC(900));




		//UPDATE ADVERTISEMENT
		updatepackble();

		err1 = bt_le_adv_update_data(ad, ARRAY_SIZE(ad),
				      NULL, 0);


		//TEST CONNESSIONE
		/*if (is_connected()){
			printk("SONOCONNESSOCICCIO\n");
		}
		else{printk("NIENTEAVENA\n");}*/



		//COMANDI BLE
		//stampa tutti i 128 bit che riceve
		int* pointerARRAY = ReadArrayBLECOMMAND();
            if(pointerARRAY[0]+pointerARRAY[1]+pointerARRAY[2]+pointerARRAY[3]>0)
            {
		printk("ARRAY ARRAY %d,%d,%d,%d\n", pointerARRAY[0],pointerARRAY[1],pointerARRAY[2],pointerARRAY[3]);

		int lunghezzaARRAY0=4;
		unsigned char arraycomando0[lunghezzaARRAY0];

		for(int i=0; i<lunghezzaARRAY0; i++){
    arraycomando0[i] = (pointerARRAY[0]>>8*i) & 0xFF;
		printk("array0 %x\n", arraycomando0[i]);}

		int lunghezzaARRAY1=4;
		unsigned char arraycomando1[lunghezzaARRAY1];

		for(int i=0; i<lunghezzaARRAY1; i++){
    arraycomando1[i] = (pointerARRAY[1]>>8*i) & 0xFF;
		printk("array1 %x\n", arraycomando1[i]);}

		int lunghezzaARRAY2=4;
		unsigned char arraycomando2[lunghezzaARRAY2];

		for(int i=0; i<lunghezzaARRAY2; i++){
    arraycomando2[i] = (pointerARRAY[2]>>8*i) & 0xFF;
		printk("array2 %x\n", arraycomando2[i]);}

		int lunghezzaARRAY3=4;
		unsigned char arraycomando3[lunghezzaARRAY3];

		for(int i=0; i<lunghezzaARRAY3; i++){
    arraycomando3[i] = (pointerARRAY[3]>>8*i) & 0xFF;
		printk("array3 %x\n", arraycomando3[i]);}

            }
            /*
            else{
                printk("No command received\n");
            }
*/

	} while (1);
    }
}
