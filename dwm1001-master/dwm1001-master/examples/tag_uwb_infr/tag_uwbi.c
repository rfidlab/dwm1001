/**
 * Copyright (c) 2019 - Frederic Mes, RTLOC
 * Copyright (c) 2015 - Decawave Ltd, Dublin, Ireland.
 *
 * This file is part of Zephyr-DWM1001.
 *
 *   Zephyr-DWM1001 is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Zephyr-DWM1001 is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Zephyr-DWM1001.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/*! --------------------------------------------------------------------------
 *  @file    bd_buzzer_main.c
 *  @brief   Double-sided two-way ranging (DS TWR) responder example code.
 *           The result is outputted via the DPS Gatt Profile.
 *
 *           This is a simple code example which acts as the responder in
 *           a DS TWR distance measurement exchange. This application waits
 *           for a "poll" message (recording the RX time-stamp of the poll)
 *           expected from the "DS TWR initiator" example code (companion
 *           to this application), and then sends a "response" message
 *           recording its TX time-stamp, after which it waits for a "final"
 *           message from the initiator to complete the exchange. The final
 *           message contains the remote initiator's time-stamps of poll TX,
 *           response RX and final TX. With this data and the local
 *           time-stamps, (of poll RX, response TX and final RX), this
 *           example application works out a value for the time-of-flight
 *           over-the-air and, thus, the estimated distance between the
 *           two devices, which it writes to console.
 *
 * @author Decawave, RTLOC
 */

// zephyr includes
#include <zephyr.h>
#include <sys/printk.h>
#include <drivers/gpio.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/util.h>
#include <time.h>
#include <string.h>

#include "rand32.h"

#include "deca_device_api.h"
#include "deca_regs.h"
#include "deca_spi.h"
#include "port.h"
//#include "dwm_api.h"

/*federico include*/
// #include "ble_device.h"
// #include <bluetooth/bluetooth.h>
// #include <bluetooth/hci.h>

#include <power/reboot.h>
/*The following define indicates whether the build is for the BeamDigital board (enabled) or for the DWM1001-DEV (commented out).*/
#define BEAMDIGITAL_BOARD 1

#define LOG_LEVEL 3
#include <logging/log.h>
LOG_MODULE_REGISTER(main);

#define MY_STACK_SIZE 512
#define MY_PRIORITY 5

K_THREAD_STACK_DEFINE(my_stack_area, MY_STACK_SIZE);

struct k_work_q my_work_q;

static u8_t mfg_data[] = { 0xff, 0xff, 0x00, 0x09,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 };
u8_t scanstruct = 0x00;

// static const struct bt_data ad[] = {
//     /*BT_DATA(BT_DATA_MANUFACTURER_DATA, mfg_data, 3),*/
//     BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
//
//     BT_DATA(BT_DATA_SVC_DATA32, mfg_data, 12)
// };

/* Example application name and version to display on console. */
#define APP_HEADER "\nDWM1001 & Zephyr\n"
#define APP_NAME "Social Distance - Dual mode with BLE\n"
#define APP_VERSION "Version - 1.0.0\n"
#define APP_VERSION_NUM 0x010400
#define APP_LINE "=================\n"

#define APP_UID 0xDECA00000000005A
#define APP_HW  1

/*Operation modes*/
#define INIT_MODE 1
#define RESP_MODE 2
#define MAX_INIT_PERIOD_MS 8000
#define MIN_INIT_PERIOD_MS 2000
#define MAX_RESP_PERIOD_MS 8000
#define MIN_RESP_PERIOD_MS 2000
static int nextSwitchTime;

/*Safe distance threshold*/

#define SAFETY_DISTANCE_THRESHOLD 0.99

/* Inter-ranging delay period, in milliseconds. */
#define RNG_DELAY_MS 1500

/* Default inter-frame delay period, in milliseconds. */
#define DFLT_TX_DELAY_MS 1000

/* Inter-frame delay period in case of RX timeout, in milliseconds.
 * In case of RX timeout, assume the receiver is not present and lower the
 * rate of blink transmission.
 */
#define RX_TO_TX_DELAY_MS 3000

/* Inter-frame delay period in case of RX error, in milliseconds.
 * In case of RX error, assume the receiver is present but its response has
 * not been received for any reason and retry blink transmission immediately.
 */
#define RX_ERR_TX_DELAY_MS 0

/* Current inter-frame delay period.
 * This global static variable is also used as the mechanism to signal events
 * to the background main loop from the interrupt handler callbacks,
 * which set it to positive delay values.
 */
static int32 tx_delay_ms = -1;

/* Default communication configuration. */
static dwt_config_t config = {
    5,               /* Channel number. */
    DWT_PRF_64M,     /* Pulse repetition frequency. */
    DWT_PLEN_128,    /* Preamble length. Used in TX only. */
    DWT_PAC8,        /* Preamble acquisition chunk size. Used in RX only. */
    9,               /* TX preamble code. Used in TX only. */
    9,               /* RX preamble code. Used in RX only. */
    1,               /* 0 to use standard SFD, 1 to use non-standard SFD. */
    DWT_BR_6M8,      /* Data rate. */
    DWT_PHRMODE_EXT, /* PHY header mode. */
    (129)            /* SFD timeout (preamble length + 1 + SFD length - PAC size).
                      Used in RX only. */
};

/* Default antenna delay values for 64 MHz PRF. See NOTE 1 below. */
#ifndef BEAMDIGITAL_BOARD
	#define TX_ANT_DLY 16436
	#define RX_ANT_DLY 16436
#else
	#define TX_ANT_DLY 16420
	#define RX_ANT_DLY 16420
#endif


/* Data types for frames used in the ranging process */
#define POLL_TYPE 0x21
#define RESP_TYPE 0x10
#define FINAL_TYPE 0x23
#define CONF_TYPE 0x24

/* Frames used in the ranging process. See NOTE 2 below. */
//static uint8 rx_poll_msg[] = {0x41, 0x88, 0, 0, 0, 0, 0, 'N', 'N',POLL_TYPE, 0, 0};
static uint8 tx_resp_msg[] = {0x41, 0x88, 0, 0, 0, 0, 0, 'N', 'N',
    RESP_TYPE, 0, 0};
//static uint8 rx_final_msg[] = {0x41, 0x88, 0, 0, 0, 0, 0, 'N', 'N',FINAL_TYPE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
//static uint8 rx_conf_msg[] = {0x41, 0x88, 0, 0, 0, 0, 0, 'N', 'N',CONF_TYPE, 0, 0, 0, 0, 0, 0};

/* Frames used in the ranging process. See NOTE 2 below. */
static uint8 tx_poll_msg[] = {0x41, 0x88, 0, 0, 0, 0, 0, 'N', 'N',
    POLL_TYPE, 0, 0};
//static uint8 rx_resp_msg[] = {0x41, 0x88, 0, 0, 0, 0, 0, 'N', 'N',RESP_TYPE, 0, 0};
static uint8 tx_final_msg[] = {0x41, 0x88, 0, 0, 0, 0, 0, 'N', 'N',
    FINAL_TYPE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
static uint8 tx_conf_msg[] = {0x41, 0x88, 0, 0, 0, 0, 0, 'N', 'N',
    CONF_TYPE, 0, 0, 0, 0, 0, 0};

/* Length of the common part of the message (up to and including the
 * function code, see NOTE 2 below).
 */
#define ALL_MSG_COMMON_LEN 10
/* Index to access some of the fields in the frames involved in the process. */
#define ALL_MSG_SN_IDX 2
#define ALL_MSG_SOURCE_ADDRESS_IDX 3
#define ALL_MSG_DEST_ADDRESS_IDX 5
#define ALL_MSG_KIND_IDX 9
#define ALL_MSG_ADDRESS_LEN 2
#define FINAL_MSG_POLL_TX_TS_IDX 10
#define FINAL_MSG_RESP_RX_TS_IDX 14
#define FINAL_MSG_FINAL_TX_TS_IDX 18
#define FINAL_MSG_TS_LEN 4

//#define RESPONSE_MSG_POLL_RX_TS_IDX 10
//#define RESPONSE_MSG_RESP_TX_TS_IDX 14
#define RESPONSE_MSG_TS_LEN 4

#define CONF_MSG_DIST_IDX 10
#define CONF_MSG_DIST_LEN 4

#define PICK_UP_PROB 0.2
#define MIN_RESPONSE_BACKOFF_DELAY_US 0 //Minimum value of delay for back off adopted by the responder before sending a RESPONSE packet
#define MAX_RESPONSE_BACKOFF_DELAY_US 24000 //Maximum value of delay for back off adopted by the responder before sending a RESPONSE packet

/* Frame sequence number, incremented after each transmission. */
static uint8 frame_seq_nb = 0;
static uint8 frame_seq_nb_rx = 0;

/* Buffer to store received messages.
 * Its size is adjusted to longest frame that this example code is
 * supposed to handle.
 */
#define RX_BUF_LEN 24
static uint8 rx_buffer[RX_BUF_LEN];

/* Hold copy of status register state here for reference so that it can be
 * examined at a debug breakpoint.
 */
static uint32 status_reg = 0;

/* UWB microsecond (uus) to device time unit (dtu, around 15.65 ps)
 * conversion factor.
 * 1 uus = 512 / 499.2 usec and 1 usec = 499.2 * 128 dtu.
 */
#define UUS_TO_DWT_TIME 65536

/* Delay between frames, in UWB microseconds. See NOTE 4 below. */

/* This is the delay from POLL RX timestamp to RESPONSE TX timestamp
 * used for calculating/setting the DW1000's delayed TX function.
 * This includes the frame length of approximately 2.46 ms with
 * above configuration.
 */
#define POLL_RX_TO_RESP_TX_DLY_UUS 8000

/* This is the delay from the end of the RESPONSE frame transmission to the enable of
 * the receiver, as programmed for the DW1000's wait for response feature.
 */
#define RESP_TX_TO_FINAL_RX_DLY_UUS 500

/* Receive final timeout. See NOTE 5 below. */
#define FINAL_RX_TIMEOUT_UUS 65000

/* Preamble timeout, in multiple of PAC size. See NOTE 6 below. */
#define PRE_TIMEOUT 30

/* This is the delay from the end of the frame transmission to the enable
 * of the receiver, as programmed for the DW1000's wait for response feature.
 */
#define POLL_TX_TO_RESP_RX_DLY_UUS 300

/* This is the delay from RESPONSE RX timestamp to  FINAL TX timestamp used
 * for calculating/setting the DW1000's delayed TX function for the FINAL frame.
 * This includes the frame length of approximately 2.66 ms with above
 * configuration.
 */
#define RESP_RX_TO_FINAL_TX_DLY_UUS 10000

/* Receive RESPONSE timeout, taking into account the maximum back off delay introduced by the responder */
#define RESP_RX_TIMEOUT_UUS MAX_RESPONSE_BACKOFF_DELAY_US+POLL_RX_TO_RESP_TX_DLY_UUS

/* Receive POLL timeout. See NOTE 5 below. */
#define POLL_RX_TIMEOUT_UUS 10000

/* Preamble timeout, in multiple of PAC size. See NOTE 6 below. */
//#define PRE_TIMEOUT 40 //LDN_CHECK: different between TX and RX

/* Timer for periodic check of BLE commands*/
#define BLE_COMMAND_READ_PERIOD_MSEC 900

/* Timer for periodic link keep pac*/
#define LINK_KEEP_PERIOD_MSEC 5000

#define LINK_KEEP_AD_DURATION_MSEC 1000

#define VIOLATION_AD_DURATION_MSEC 3000

#define LEDS_OFF_TENTATIVE_MSEC 8000

#define LEDS_OFF_SEC 40

#define LED_LK_OFF_MSEC 50

#define SOS_AD_OFF_SEC 30
#define SOS_LED_OFF_SEC 6

#define AUTO_RESET_PERIOD_SEC 3600

#ifdef BEAMDIGITAL_BOARD

#define BUZZER_OFF_MSEC 300

#endif

#define ADV_TYPE_LINK_KEEP 1
#define ADV_TYPE_VIOLATION_NOTIFICATION 2
#define ADV_TYPE_CONNECTION_REQUEST 3
#define ADV_TYPE_SOS 4

#define N_DETECTABLE_TAGS 10
#define MAX_VIOLATION_COUNT 3

/* Dummy buffer for DW1000 wake-up SPI read. See NOTE 3 below. */
#define DUMMY_BUFFER_LEN 600
static uint8 dummy_buffer[DUMMY_BUFFER_LEN];

/* Timestamps of frames transmission/reception.
 * As they are 40-bit wide, we need to define a 64-bit int
 * type to handle them.
 */
typedef signed long long int64;
typedef unsigned long long uint64;

static uint64 poll_rx_ts;
static uint64 resp_tx_ts;
static uint64 final_rx_ts;

static uint64 poll_tx_ts;
static uint64 resp_rx_ts;
static uint64 final_tx_ts;
static int confirmationMessageOn=1;
static int numViolations=0;

/* Variable storing the ID of the tag */
static uint16 myID;


/* Variable storing flag for ads reporting a user inititated SOS request */
static int SOS_AD_ON;

/* Variable storing flag for led on after a user inititated SOS request */
static int SOS_LED_ON;

/* Variable storing flag for ads broadcasting a user initiated connection request*/
static int CONNECTION_REQUEST_ON;

/* Variable storing flag for ads reporting violation */
static int VIOLATION_AD_ON;

/* Variable storing flag for ads broadcasting link keep*/
static int BLE_LK_ON;


static int VIOLATION_FLAG;
static int LEDS_OFF_TENTATIVE;

/*Variable storing flag for accuracy tests*/
static int accuracyTests;

/* Speed of light in air, in metres per second. */
#define SPEED_OF_LIGHT 299702547

/*LED related section*/

/*
 * See ./build/zephyr/include/generated/generated_dts_board.conf for details
 */
#define GPIO_DRV_NAME       DT_INST_0_NORDIC_NRF_GPIO_LABEL
#define GPIO_OUT_PIN_RED    DT_ALIAS_LED0_GPIOS_PIN
#define GPIO_OUT_PIN_GREEN  DT_ALIAS_LED1_GPIOS_PIN
#define GPIO_OUT_PIN_BLUE   DT_ALIAS_LED2_GPIOS_PIN
#define GPIO_IN_PIN_BUTTON  DT_ALIAS_SW0_GPIOS_PIN

#ifndef BEAMDIGITAL_BOARD

#define GPIO_OUT_PIN_RED2   DT_ALIAS_LED3_GPIOS_PIN

#else

#define GPIO_OUT_PIN_BUZZER   DT_ALIAS_LED3_GPIOS_PIN

#endif

static struct gpio_callback gpio_cb;
struct device * gpiob;

/*Timer for operation mode switch*/

struct k_timer my_timer;


/*Timer to check if BLE commands were issued*/
struct k_timer BLE_command_timer;

/*Timer for transmission of link keep packets*/
struct k_timer link_keep_timer;

/*Timer for automatic shutdown of ad packets for link keep*/
struct k_timer link_keep_ad_timer;

/*Timer for automatic shutdown of ad packets for distance violation*/
struct k_timer violation_ad_timer;

/*Timer for automatic leds turn off if no violation is detected for a while, but no further measurement is carried out*/
struct k_timer leds_off_timer;


/*Timer for automatic red led turn off after link keep*/
struct k_timer led_lk_off_timer;
#ifdef BEAMDIGITAL_BOARD
/*Timer for automatic buzzer turn off after violation*/
struct k_timer buzzer_off_timer;

#endif


#ifndef BEAMDIGITAL_BOARD
void led_red1_off(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_RED, 1);
}
void led_green_off(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_GREEN, 1);
}

void led_blue_off(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_BLUE, 1);
}
void led_red2_off(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_RED2, 1);
}
void led_red1_on(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_RED, 0);
}

void led_green_on(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_GREEN, 0);
}

void led_blue_on(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_BLUE, 0);
}

void led_red2_on(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_RED2, 0);
}
#else
void led_red1_off(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_RED, 0);
}
void led_green_off(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_GREEN, 0);
}

void led_blue_off(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_BLUE, 0);
}

void buzzer_off(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_BUZZER, 0);
}

void led_red1_on(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_RED, 1);
}

void led_green_on(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_GREEN, 1);
}

void led_blue_on(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_BLUE, 1);
}

void buzzer_on(void)
{
    gpio_pin_set(gpiob, GPIO_OUT_PIN_BUZZER, 1);
}

#endif
/*void update_ble_packet_SOS(){
 int err;
 //printk("primoelemento %d", mfg_data[1]);
 uint8_t splittedOTA[2];
 splittedOTA[0]=myID & 0xff;
 splittedOTA[1]=(myID >> 8);
 //pacchetto advertisement, indirizzo + distanza in ASCII
 mfg_data[0]=ADV_TYPE_SOS;
 mfg_data[1]=splittedOTA[0];
 mfg_data[2]=splittedOTA[1];
 err = bt_le_adv_update_data(ad, ARRAY_SIZE(ad), NULL, 0);
 printk("adv update SOS %d \n",err);
 }
 */
/*Timer for automatic shutdown of ad packets for distance violation*/
struct k_timer SOS_ad_timer;
static int previousButtonState=1;
static s64_t buttonPressureTime,buttonPressureDuration;
static int sleepingOn=0;
static int sleepingRequested=0;
/* Button event callback */
static void button_event(struct device * gpiob,
                         struct gpio_callback * cb,
                         u32_t pins)
{
    int button_state;
    //int err;
    int actionCompleted=0;
    button_state = gpio_pin_get(gpiob, GPIO_IN_PIN_BUTTON);
    if (sleepingOn) {
        printk("Button pressed while sleeping, waking up chip\n");
        //dwt_spicswakeup(dummy_buffer, DUMMY_BUFFER_LEN);
        sleepingOn=0;
        //dwt_configure(&config);
        //dwt_setrxaftertxdelay(POLL_TX_TO_RESP_RX_DLY_UUS);
        //dwt_setrxtimeout(RESP_RX_TIMEOUT_UUS);
        k_timer_start(&link_keep_timer, K_MSEC(LINK_KEEP_PERIOD_MSEC), K_NO_WAIT);
        return;
    }
    if((button_state==0)&&(previousButtonState==1))
    {
        buttonPressureTime=k_uptime_get();
        previousButtonState=0;
        printk("Button pressed, starting to count\n");
    }
    else
    {
        if((button_state==1)&&(previousButtonState==0))
        {
            buttonPressureDuration=k_uptime_delta(&buttonPressureTime);
            printk("Button released, checking pressure duration\n");
            previousButtonState=1;
            actionCompleted=1;
        }
        else
        {
            printk("Wrong state, ignoring\n");
            return;
        }
    }
    if(actionCompleted)
    {
        if (buttonPressureDuration<1000)
        {
            printk("Pressure duration: %lld ms. SOS! Starting advertisement\n",buttonPressureDuration);
            SOS_AD_ON=1;
            SOS_LED_ON=1;
            k_timer_start(&SOS_ad_timer, K_SECONDS(SOS_LED_OFF_SEC), K_NO_WAIT);
            led_red1_on();

        }
        int resetButton=1;
        if(resetButton)
        {
        	if (buttonPressureDuration>5000)
        	{
        		#ifdef BEAMDIGITAL_BOARD
            		buzzer_on();
            	#endif
            	sys_reboot(SYS_REBOOT_WARM);
        	}
        }
        else
        {
        if (buttonPressureDuration>3000)
        {
            printk("Pressure duration: %lld ms. Sleep request detected\n",buttonPressureDuration);
            #ifdef BEAMDIGITAL_BOARD
            buzzer_on();
            #endif

            sleepingRequested=1;
        }
        }

    }
    //printk("Button %s cycle count %u\n", button_state ?  "released:" : "pressed: ", k_cycle_get_32());


    /*err = bt_le_adv_start(BT_LE_ADV_NCONN, ad, ARRAY_SIZE(ad),
     NULL, 0);
     if(err)
     {
     printk("SOS! Advertisement start error %d\n",err);
     }*/
    //update_ble_packet_SOS();
    /* led_red1_on();
     k_sleep(K_MSEC(100));
     led_red1_off();
     k_sleep(K_MSEC(10));
     led_red1_on();
     k_sleep(K_MSEC(100));
     led_red1_off();*/
}


/* Hold copies of computed time of flight and distance here for reference
 * so that it can be examined at a debug breakpoint.
 */
static double tof_static;
static double distance_static;
static double averageDistance;
static int numDistances;
/* String used to display measured distance on console. */
char dist_str[24] = {0};

/* Declaration of static functions. */
static uint64 get_tx_timestamp_u64(void);
static uint64 get_rx_timestamp_u64(void);
static void final_msg_get_ts(const uint8 * ts_field, uint32 * ts);
static void final_msg_set_ts(uint8 *ts_field, uint64 ts);
//static void response_msg_get_ts(const uint8 * ts_field, uint32 * ts);
//static void response_msg_set_ts(uint8 *ts_field, uint64 ts);
static void conf_msg_get_distance(uint8 *ts_field, uint32 *dist);
static void conf_msg_set_distance(uint8 *ts_field, uint32 distance);
static void msg_set_src_address(uint8 *ts_field, const uint16 address);
static void msg_set_dest_address(uint8 *ts_field, const uint16 address);

static void rx_ok_cb(const dwt_cb_data_t *cb_data);
static void rx_to_cb(const dwt_cb_data_t *cb_data);
static void rx_err_cb(const dwt_cb_data_t *cb_data);
static void tx_conf_cb(const dwt_cb_data_t *cb_data);
static void getMyID(uint16 *tagID);
static void recPacketProcess(uint8 *rx_buffer, int *packet_kind, uint16 *packet_source, uint16 *packet_destination, int *packet_poll_request_nb);
static int recPacketCheck(int current_mode, uint16 currentInitID, uint16 currentRespID, int current_poll_request_nb, int packet_kind, uint16 packet_source, uint16 packet_destination, int packet_poll_request_nb);

static volatile uint8_t process_isr = 0;

/* Variable storing current operation mode for the device */
static int OP_MODE;

/* Variable storing flag for transmission of response packet */
static int RESP_TO_TIMEOUT;


/*Vector used to store the IDs of tags that exchanged measurements
 with this tag*/
static uint16 detectedTagIDs[N_DETECTABLE_TAGS];

/*Vector used to keep track if each detected tag was above or below the threshold  in the last measurement with this tag*/
static int violationTagFlag[N_DETECTABLE_TAGS];

//extern void op_mode_switch(struct k_timer *timer_id);


int returnBatteryLevel()
{
    double batteryVoltage,chipTemp;
    int batteryLevel;
    char outStr[128];
    uint16 tempAndVoltage;
    tempAndVoltage=dwt_readtempvbat(1);
    batteryVoltage= 0.0057 *(tempAndVoltage & 0xff)+2.3;
    chipTemp = 1.13 * ((tempAndVoltage & 0xFF00) >> 8) - 113.0;
    sprintf(outStr,"Battery voltage = %lf V\n", batteryVoltage );
    printk("%s",outStr);
    sprintf(outStr,"IC temperature = %lf Celsius degrees\n", chipTemp);
    printk("%s",outStr);

    /*TBD: write function that retrieves battery level*/
    batteryLevel=50;
    return batteryLevel;
}

/// todo: definire pacchetti notify anche per altre situazione tipo link keep
void ble_notify_link_keep() {
  /* BLE Configuration */
  // ble_lks_t *ble_lks;
  // uint8_t ble_buf [120] = {0};
  //
  // ble_lks = (ble_lks_t *)(&ble_buf[0]);
  //
  // k_yield();
  //
  // if(is_connected()) {
  //   /*We send up a BLE notification if the minimum distance threshold is violated*/
  //   ble_lks->cnt = 1;
  //   if(SOS_AD_ON) {
  //     ble_lks->ble_lk[0].lk_type = ADV_TYPE_SOS;
  //   } else {
  //     ble_lks->ble_lk[0].lk_type = ADV_TYPE_LINK_KEEP;
  //   }
  //
  //   ble_lks->ble_lk[0].my_id = myID;
  //   ble_lks->ble_lk[0].collisions = numViolations;  //tmp
  //   ble_lks->ble_lk[0].battery = returnBatteryLevel();     //tmp
  //   ble_lks->ble_lk[0].tqf = 0;
  //
  //   dwm1001_notify((uint8_t*)ble_buf, 1 + sizeof(ble_lk_t) * ble_lks->cnt);
  //
  //   printk("Notified\n");
  // } else {
  //   printk("Not connected, avoiding notify\n");
  // }
}

void update_ble_packet_LK(){
  ble_notify_link_keep();
  /// @Generoso: commented
    // int err;
    // //printk("primoelemento %d", mfg_data[1]);
    // uint8_t splittedOTA[2];
    // splittedOTA[0]=myID & 0xff;
    // splittedOTA[1]=(myID >> 8);
    // //pacchetto advertisement, indirizzo + distanza in ASCII
    // if(SOS_AD_ON)
    // {
    //     mfg_data[0]=ADV_TYPE_SOS;
    // }
    // else
    // {
    //     mfg_data[0]=ADV_TYPE_LINK_KEEP;
    // }
    // mfg_data[1]=splittedOTA[0];
    // mfg_data[2]=splittedOTA[1];
    // mfg_data[3]=numViolations;
    // mfg_data[4]=(uint8_t) returnBatteryLevel();
    // mfg_data[5]=0; // @generoso
    // mfg_data[6]=0; // @generoso
    // mfg_data[7]=0; // @generoso
    // mfg_data[8]=0; // @generoso
    // mfg_data[9]=0; // @generoso
    // mfg_data[10]=0; // @generoso
    // mfg_data[11]=0; // @generoso

    ///todo: aggiungere un notify
    // err = bt_le_adv_update_data(ad, ARRAY_SIZE(ad),
    //                             NULL, 0);
    // printk("adv update %d \n",err);
}

void mode_switch_handler(struct k_work *work)
{

    /// @Generoso: for UWBI Tag only, responder mode only
    if(myID >= 65000) {
      OP_MODE=RESP_MODE;
      nextSwitchTime=(((uint32)sys_rand32_get()) % (MAX_RESP_PERIOD_MS - MIN_RESP_PERIOD_MS + 1)) + MIN_RESP_PERIOD_MS;
      return;
    }

    if(OP_MODE==INIT_MODE)
    {
        //printk("Switching mode: from INIT to RESP\n");
        OP_MODE=RESP_MODE;
        nextSwitchTime=(((uint32)sys_rand32_get()) % (MAX_RESP_PERIOD_MS - MIN_RESP_PERIOD_MS + 1)) + MIN_RESP_PERIOD_MS;
    }
    else
    {
        //printk("Switching mode: from RESP to INIT\n");
        OP_MODE=INIT_MODE;
        nextSwitchTime=(((uint32)sys_rand32_get()) % (MAX_INIT_PERIOD_MS - MIN_INIT_PERIOD_MS + 1)) + MIN_INIT_PERIOD_MS;
    }
    k_timer_start(&my_timer, K_MSEC(nextSwitchTime), K_NO_WAIT);
}

K_WORK_DEFINE(my_work, mode_switch_handler);
void my_timer_handler(struct k_timer *dummy)
{
    k_work_submit(&my_work);
}


/*Timer for periodic reset*/
struct k_timer auto_reset_timer;
void auto_reset_handler(struct k_work *work)
{
    printk("RESET\n");
    //k_timer_start(&auto_reset_timer, K_SECONDS(AUTO_RESET_PERIOD_SEC), K_NO_WAIT);
    sys_reboot(SYS_REBOOT_WARM);
}

K_WORK_DEFINE(auto_reset_work, auto_reset_handler);
void auto_reset_timer_handler(struct k_timer *dummy)
{
    k_work_submit(&auto_reset_work);
}

/*Timer for transmission of response packet */

struct k_timer resp_TO_timer;
void resp_TO_handler(struct k_work *work)
{
    //printk("Response timeout expired\n");
    RESP_TO_TIMEOUT=1;
}

K_WORK_DEFINE(resp_TO_work, resp_TO_handler);
void resp_TO_timer_handler(struct k_timer *dummy)
{
    k_work_submit(&resp_TO_work);
}

/*void op_mode_switch(struct k_timer *timer_id){
 if(OP_MODE==INIT_MODE)
 {
 printk("Switching mode: from INIT to RESP\n");
 OP_MODE=RESP_MODE;
 }
 else
 {
 printk("Switching mode: from RESP to INIT\n");
 OP_MODE=INIT_MODE;
 }
 }

 */



void BLE_command_handler(struct k_work *work)
{
    //printk("BLE_command_check timeout expired\n");
    /*To be added here: function that reads command and if needed stores it and raises a flag*/
    k_timer_start(&BLE_command_timer, K_MSEC(BLE_COMMAND_READ_PERIOD_MSEC), K_NO_WAIT);
}

K_WORK_DEFINE(BLE_command_work, BLE_command_handler);
void BLE_command_timer_handler(struct k_timer *dummy)
{
    k_work_submit(&BLE_command_work);
}

void link_keep_handler(struct k_work *work)
{
    int err;
    //printk("Link keep timeout expired\n");
    if(sleepingOn)
    {
        printk("Sleep mode, stopping the transmission of Link Keep packets\n");
        return;
    }
    if((VIOLATION_AD_ON==0) && (CONNECTION_REQUEST_ON==0) &&(SOS_AD_ON==0))
    {
        BLE_LK_ON=1;


        // err = bt_le_adv_start(BT_LE_ADV_NCONN, ad, ARRAY_SIZE(ad),
        //                       NULL, 0);
        // if(err==0)
        // {
        //     printk("Link keep advertisement started\n");
        // }
        // else
        // {
        //     printk("Link keep advertising failed to start (err %d)\n", err);
        // }
        update_ble_packet_LK();
        k_timer_start(&link_keep_ad_timer, K_MSEC(LINK_KEEP_AD_DURATION_MSEC), K_NO_WAIT);
        led_red1_on();
        k_timer_start(&led_lk_off_timer, K_MSEC(LED_LK_OFF_MSEC), K_NO_WAIT);
    }
    else
    {
        ;//printk("Link keep advertisement skipped, something else going on\n");
    }

    k_timer_start(&link_keep_timer, K_MSEC(LINK_KEEP_PERIOD_MSEC), K_NO_WAIT);
}

K_WORK_DEFINE(link_keep_work, link_keep_handler);
void link_keep_timer_handler(struct k_timer *dummy)
{
    k_work_submit(&link_keep_work);
}


void link_keep_ad_handler(struct k_work *work)
{
    //printk("Link keep advertisement timeout expired\n");
    /*we stop the advertisement started when the last link keep timeout expired, unless there is a violation being advertised or a connection request*/
    BLE_LK_ON=0;
    if((VIOLATION_AD_ON==0) && (CONNECTION_REQUEST_ON==0) &&(SOS_AD_ON==0))
    {
        //printk("Stopping advertisement\n");
        /// @Generoso: commented
        // ble_stop_advertising();
    }
    else
    {
        ;//printk("Advertisement not stopped, something else is going on\n");
    }
}

K_WORK_DEFINE(link_keep_ad_work, link_keep_ad_handler);
void link_keep_ad_timer_handler(struct k_timer *dummy)
{
    k_work_submit(&link_keep_ad_work);
}

void violation_ad_handler(struct k_work *work)
{
    //printk("Violation advertisement timeout expired\n");
    /*we stop the advertisement started when the last violation timeout expired, unless there is a violation being advertised or a connection request*/
    VIOLATION_AD_ON=0;
    if((CONNECTION_REQUEST_ON==0) &&(SOS_AD_ON==0))
    {
        //printk("Stopping advertisement\n");
        /// @Generoso: commented
        // ble_stop_advertising();
    }
    else
    {
        ;//printk("Advertisement not stopped, something else is going on\n");
    }
}

K_WORK_DEFINE(violation_ad_work, violation_ad_handler);
void violation_ad_timer_handler(struct k_timer *dummy)
{
    k_work_submit(&violation_ad_work);
}

void leds_off_handler(struct k_work *work)
{
    printk("Leds off timer expired\n");
    /*If LEDS_OFF_TENATIVE is set to 1, there were no distance violations in the last LEDS_OFF_TENTATIVE_MSEC, so we turn off the leds (without checking, they moight already be off, but we don't care). If LEDS_OFF_TENATIVE is set to 0, we set it to 1, and come back to check later.*/
    if (LEDS_OFF_TENTATIVE==1)
    {
        printk("leds off\n");
        /*If we turn the leds off, we also have to reset the vectors recording violations, in order to ensure consistency*/
        int tCounter;
        for (tCounter=0;tCounter<N_DETECTABLE_TAGS;tCounter=tCounter+1)
        {
            detectedTagIDs[tCounter]=0;
            violationTagFlag[tCounter]=0;
        }
        VIOLATION_FLAG=0;
        led_green_off();
        led_blue_off();
        led_red1_off();
#ifndef BEAMDIGITAL_BOARD
        led_red2_off();
#endif
    }
    else
    {
        printk("LEDS_OFF_TENTATIVE set to %d, leds left on\n",LEDS_OFF_TENTATIVE);
        LEDS_OFF_TENTATIVE=1;
        k_timer_start(&leds_off_timer, K_SECONDS(LEDS_OFF_SEC), K_NO_WAIT);

    }
}

K_WORK_DEFINE(leds_off_work, leds_off_handler);
void leds_off_timer_handler(struct k_timer *dummy)
{
    k_work_submit(&leds_off_work);
}

void led_lk_off_handler(struct k_work *work)
{

    /*If VIOLATION_FLAG is set to 0, there were no recent distance violations so we turn off the red led (without checking, it might already be off, but we don't care).*/
    if(VIOLATION_FLAG==0)
    {
        printk("Led off after link keep timer expired\n");
        led_red1_off();
    }
    else
    {
        printk("Led left on, violation detected\n");
    }
}

K_WORK_DEFINE(led_lk_timer_work, led_lk_off_handler);
void led_lk_timer_handler(struct k_timer *dummy)
{
    k_work_submit(&led_lk_timer_work);
}
#ifdef BEAMDIGITAL_BOARD
void buzzer_off_handler(struct k_work *work)
{
    printk("Buzzer timer expired - buzzer off \n");
    buzzer_off();
}

K_WORK_DEFINE(buzzer_off_timer_work, buzzer_off_handler);
void buzzer_off_timer_handler(struct k_timer *dummy)
{
    k_work_submit(&buzzer_off_timer_work);
}

#endif
void SOS_ad_handler(struct k_work *work)
{

    if(SOS_LED_ON)
    {
        led_red1_off();
        SOS_LED_ON=0;
        printk("SOS led off\n");
        k_timer_start(&SOS_ad_timer, K_SECONDS(SOS_AD_OFF_SEC), K_NO_WAIT);
    }
    else
    {
        /*We set tbe SOS FLAG to 0, but we leave the advertisement on: the next event of any kind will take control of it*/
        SOS_AD_ON=0;
        printk("SOS ad flag off\n");
    }


}

K_WORK_DEFINE(SOS_ad_timer_work, SOS_ad_handler);
void SOS_ad_timer_handler(struct k_timer *dummy)
{
    k_work_submit(&SOS_ad_timer_work);
}


void print_reg(int register_ID) {
    int c,k;
    if(register_ID==SYS_STATUS_ID)
        printk("STATUS REGISTER CONTENT: ");

    if(register_ID==TX_FCTRL_ID)
        printk("TX_FCTRL CONTENT: ");

    uint32 register_content=dwt_read32bitreg(register_ID);
    for (c = 31; c >= 0; c--)
    {
        if ((c+1)%8==0)
            printk(" ");
        k = register_content >> c;
        if (k & 1)
            printk("1");
        else
            printk("0");
    }

    printk("\n");
}




double sd_init_mode(uint16 *otherTagAddress) {
    /* Write frame data to DW1000 and prepare transmission.
     * See NOTE 8 below.
     */
    double distance;
    //double tof;
    //double Ra;
    //double Da;
    int poll_frame_nb;
    int ret;
    uint16 currentInitID;
    uint16 currentRespID;
    uint16 srcAddress;
    uint16 destAddress;
    int packet_poll_request_nb;
    int currentMode=INIT_MODE;
    int packetKind;

    currentInitID=myID;
    currentRespID=0;
    //int64 tof_dtu;
    /*LDN: the following line should be replaced with a function call that sets all
     * the relevant information in tha packet, instead of using a prewritten buffer
     * and just setting the frame sequence number
     */
    tx_poll_msg[ALL_MSG_SN_IDX] = frame_seq_nb;
    msg_set_src_address(tx_poll_msg, myID);
    msg_set_dest_address(tx_poll_msg, 65535);
    //printk("%s\n",tx_poll_msg);
    /* Zero offset in TX buffer. */
    dwt_writetxdata(sizeof(tx_poll_msg), tx_poll_msg, 0);

    /* Zero offset in TX buffer, ranging. */
    dwt_writetxfctrl(sizeof(tx_poll_msg), 0, 1);

    /* Start transmission, indicating that a response is expected so that
     * reception is enabled automatically after the frame is sent and the
     * delay set by dwt_setrxaftertxdelay() has elapsed.
     */
    //print_reg(SYS_STATUS_ID);
    /* Clear good RX frame event and TX frame sent in the DW1000
     * status register.
     */
    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_ALL_TX);
    //print_reg(SYS_STATUS_ID);
    //print_reg(TX_FCTRL_ID);
    ret=dwt_starttx(DWT_START_TX_IMMEDIATE | DWT_RESPONSE_EXPECTED);

    /* LDN: from here code added to replace register polling with interrupt polling*/
    //     printk("Process ISR  before starttx = %u\n",process_isr);
    //     dwt_setrxtimeout(0);
    //     dwt_starttx(DWT_START_TX_IMMEDIATE | DWT_RESPONSE_EXPECTED);
    //     printk("POLL packet (%u) sent\n",frame_seq_nb);
    //     while (process_isr != 1) {
    //         // Wait for interrupt because of TX event to happen.
    //     }
    //     printk("Process ISR after first while = %u\n",process_isr);
    //     /* Run dwt_isr */
    //     /* Note: this is out of the real ISR on purpose because zephyr
    //      * doesn't like APIs like spi being used during the ISR
    //      */
    //     dwt_isr();
    //
    /* reset variable to 0 */
    //     process_isr = 0;
    //      printk("Process ISR before second while = %u\n",process_isr);
    //    while (process_isr != 1) {
    //        // Wait for interrupt because of RX event to happen.
    //    }
    //         printk("Process ISR after second while = %u\n",process_isr);
    //    /* Run dwt_isr */
    //    /* Note: this is out of the real ISR on purpose because zephyr
    //     * doesn't like APIs like spi being used during the ISR
    //     */
    //    dwt_isr();

    /* reset variable to 0 */
    //    process_isr = 0;
    /* Until here: code to to replace register polling with interrupt polling*/

    /*LDN: Code to be commented out, if replaced by interrupt polling */
    if (ret == DWT_SUCCESS) {
        //printk("POLL packet sent\n");
        /* Poll DW1000 until TX frame sent event set.
         * See NOTE 9 below.
         */
        //print_reg(SYS_STATUS_ID);
        //print_reg(TX_FCTRL_ID);
        //int txlen=dwt_read32bitreg(SYS_STATUS_ID) & TX_FCTRL_TFLEN_MASK;
        //printk("TXLEN: %u\n",txlen);
        while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
        {};

        /* Clear TXFRS event. */
        dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);

        //printk("success (%u)\n", frame_seq_nb);

        /* Increment frame sequence number after transmission of the poll
         * message (modulo 256).
         */

        poll_frame_nb=frame_seq_nb;
        frame_seq_nb++;

        //return(distance);
    }
    else {
        printk("POLL message sending error - tx failed: %08lx\n", status_reg);
        return(-1);
    }

    /* We assume that the transmission is achieved correctly, poll for
     * reception of a frame or error/timeout. See NOTE 9 below.
     */
    while (!((status_reg = dwt_read32bitreg(SYS_STATUS_ID)) &
             (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_TO | SYS_STATUS_ALL_RX_ERR)))
    { };
    /* Code to be commented out, if replaced by interrupt polling until here */



    if (status_reg & SYS_STATUS_RXFCG) {
        uint32 frame_len;

        /* Clear good RX frame event and TX frame sent in the DW1000
         * status register.
         */
        dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_RXFCG | SYS_STATUS_TXFRS);

        /* A frame has been received, read it into the local buffer. */
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFLEN_MASK;
        if (frame_len <= RX_BUF_LEN) {
            dwt_readrxdata(rx_buffer, frame_len, 0);
        }

        /* Check that the frame is the expected response from the companion
         * "DS TWR responder" example.
         * As the sequence number field of the frame is not relevant,
         * it is cleared to simplify the validation of the frame.
         */
        recPacketProcess(rx_buffer, &packetKind, &srcAddress, &destAddress, &packet_poll_request_nb);
        int checkResult=recPacketCheck(currentMode, currentInitID, currentRespID, poll_frame_nb, packetKind, srcAddress, destAddress, packet_poll_request_nb);
        rx_buffer[ALL_MSG_SN_IDX] = 0;
        if(checkResult) {

            currentRespID=srcAddress;
            //printk("Response packet received\n")
            uint32 final_tx_time;
            //          uint32 poll_rx_ts_32, resp_tx_ts_32, poll_tx_ts_32, resp_rx_ts_32;
            //int ret;
            /* Retrieve poll transmission and response reception timestamp. */
            poll_tx_ts = get_tx_timestamp_u64();
            resp_rx_ts = get_rx_timestamp_u64();

            /*LDN: this part was added to enable the INIT node to perform its own
             distance estimation, but mostly for debugging reasons: this estimate is not
             used, since the estimate sent by the RESP node will be way more accurate.
             Note that in order to enable this code one has to increase the length of
             RESPONSE frame of at least 8 bytes to make room for the time stamps, and
             uncomment the response_msg_get_ts() response_msg_set_ts() functions and the
             support uint variables commented a few lines above this.*/

            /* Get timestamps embedded in the response message. */
            //            response_msg_get_ts(&rx_buffer[RESPONSE_MSG_POLL_RX_TS_IDX], &poll_rx_ts_32);
            //             response_msg_get_ts(&rx_buffer[RESPONSE_MSG_RESP_TX_TS_IDX], &resp_tx_ts_32);
            //
            //             /* Compute time of flight. 32-bit subtractions give
            //              * correct answers even if clock has wrapped.
            //              * See NOTE 12 below.
            //              */
            //             poll_tx_ts_32 = (uint32)poll_tx_ts;
            //             resp_rx_ts_32 = (uint32)resp_rx_ts;
            //
            //             Ra = (double)(resp_rx_ts_32 - poll_tx_ts_32);
            //             Da = (double)(resp_tx_ts_32 - poll_rx_ts_32);
            //
            //             sprintf(dist_str, "Ra: %f, Da: %f\n",
            //                     Ra, Da);
            //             printk("%s", dist_str);
            //
            //             tof_dtu = (int64)((Ra  - Da) / 2);
            //
            //             tof = tof_dtu * DWT_TIME_UNITS;
            //             distance = tof * SPEED_OF_LIGHT;
            //             tof_static=tof;
            //             distance_static=distance;
            //             /* Display computed distance on console. */
            //             sprintf(dist_str, "dist (%u): %3.2f m\n",
            //                     frame_seq_nb_rx, (float)(distance));
            //             printk("%s", dist_str);
            /*LDNCHECK: until here*/



            /* Compute final message transmission time. See NOTE 10 below. */
            final_tx_time = (resp_rx_ts +
                             (RESP_RX_TO_FINAL_TX_DLY_UUS * UUS_TO_DWT_TIME)) >> 8;

            dwt_setdelayedtrxtime(final_tx_time);

            /* Final TX timestamp is the transmission time we programmed
             * plus the TX antenna delay.
             */
            final_tx_ts = (((uint64)(final_tx_time & 0xFFFFFFFEUL)) << 8) +
            TX_ANT_DLY;

            /* Write all timestamps in the final message.
             * See NOTE 11 below.
             */
            final_msg_set_ts(&tx_final_msg[FINAL_MSG_POLL_TX_TS_IDX], poll_tx_ts);
            final_msg_set_ts(&tx_final_msg[FINAL_MSG_RESP_RX_TS_IDX], resp_rx_ts);
            final_msg_set_ts(&tx_final_msg[FINAL_MSG_FINAL_TX_TS_IDX], final_tx_ts);

            /* Write and send final message.
             * See NOTE 8 below.
             */
            tx_final_msg[ALL_MSG_SN_IDX] = poll_frame_nb;
            msg_set_src_address(tx_final_msg, myID);
            msg_set_dest_address(tx_final_msg, currentRespID);
            /* Zero offset in TX buffer. */
            dwt_writetxdata(sizeof(tx_final_msg), tx_final_msg, 0);

            /* Zero offset in TX buffer, ranging. */
            dwt_writetxfctrl(sizeof(tx_final_msg), 0, 1);

            /*LDN: added here code to wait for confirmation packet RESP->INIT,
             including the estimated distance to be extracted and returned. We do this for at least 2 reasons:
             1) Distance estimation at the RESP node will be more accurate, as it is based on a 3-packets
             exchange (see DWM1000 User Manual);
             2) We want the same distance to be shared by both nodes, in order to avoid inconsistencies in
             determining violations of minimum safeety distances;
             Note that we could perform an additional distance estimation by asking the RESP to provide the required TimeStamps,
             but this is not done in order to avoid inconsistencies (see point 2) above) and to save time, because in this way the
             RESP node can send the confirmation packet immediately. */

            if(confirmationMessageOn)
            {
                ret = dwt_starttx(DWT_START_TX_DELAYED | DWT_RESPONSE_EXPECTED);
                /*LDN: the block of code below is for debugging, as transmission seems to hang here when DWT_RESPONSE_EXPECTED is used*/
                if (ret == DWT_SUCCESS) {
                    //printk("Final packet sent\n");
                    /* Poll DW1000 until TX frame sent event set.
                     * See NOTE 9 below.
                     */
                    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                    { };

                    /* Clear TXFRS event. */
                    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);

                    //printk("success (%u)\n", frame_seq_nb);

                    /* Increment frame sequence number after transmission of
                     * the final message (modulo 256).
                     */
                    frame_seq_nb++;

                    //return(distance);
                }
                else {
                    printk("FINAL message sending error - tx failed: %08lx\n", status_reg);
                    return(-1);
                }
                /*until here*/

                /* We assume that the transmission is achieved correctly, poll for
                 * reception of a frame or error/timeout. See NOTE 9 below.
                 */
                while (!((status_reg = dwt_read32bitreg(SYS_STATUS_ID)) &
                         (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_TO | SYS_STATUS_ALL_RX_ERR)))
                { };
                //printk("Out of the while\n");
                /* Increment frame sequence number after transmission of the poll
                 * message (modulo 256).
                 */
                //frame_seq_nb++;

                if (status_reg & SYS_STATUS_RXFCG) {
                    uint32 frame_len;
                    uint32 dist_int_received;
                    float distance_received;
                    /* Clear good RX frame event and TX frame sent in the DW1000
                     * status register.
                     */
                    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_RXFCG | SYS_STATUS_TXFRS);

                    /* A frame has been received, read it into the local buffer. */
                    frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFLEN_MASK;
                    if (frame_len <= RX_BUF_LEN) {
                        dwt_readrxdata(rx_buffer, frame_len, 0);
                    }

                    /* Check that the frame is the expected CONFIRM packet from the companion
                     * "DS TWR responder" example.
                     * As the sequence number field of the frame is not relevant,
                     * it is cleared to simplify the validation of the frame.
                     */
                    recPacketProcess(rx_buffer, &packetKind, &srcAddress, &destAddress, &packet_poll_request_nb);
                    int checkResult=recPacketCheck(currentMode, currentInitID, currentRespID, poll_frame_nb, packetKind, srcAddress, destAddress, packet_poll_request_nb);
                    rx_buffer[ALL_MSG_SN_IDX] = 0;
                    if(checkResult) {
                        conf_msg_get_distance(&rx_buffer[CONF_MSG_DIST_IDX], &dist_int_received);
                        distance_received=((double) dist_int_received)/1000;
                        //printk("CONFIRM packet received\n");
                        //printk("Success (%u)\n", poll_frame_nb);
                        // if(checkResult)
                        //                                 printk("Check ok\n");
                        //                         else
                        //                             printk("Check not ok\n");
                        return(distance_received);
                    }
                    else
                    {
                        //printk("Error receiving CONFIRM packet\n");
                        return(-1);
                    }
                }
                else {
                    printk("Timeout/failure while waiting for CONFIRM packet\n");
                    return(-1);
                }
            }
            else
            {
                ret = dwt_starttx(DWT_START_TX_DELAYED);
                //return(distance);



                /* If dwt_starttx() returns an error, abandon this ranging
                 * exchange and proceed to the next one. See NOTE 12 below.
                 */

                if (ret == DWT_SUCCESS) {
                    /* Poll DW1000 until TX frame sent event set.
                     * See NOTE 9 below.
                     */
                    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                    { };

                    /* Clear TXFRS event. */
                    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);

                    //printk("Success (%u)\n", poll_frame_nb);

                    /* Increment frame sequence number after transmission of
                     * the final message (modulo 256).
                     */
                    frame_seq_nb++;
                    *otherTagAddress=currentRespID;
                    return(distance);
                }
                else {
                    printk("FINAL message sending error - tx failed: %08lx\n", status_reg);
                    return(-1);
                }
            }
        }
        else {
            printk("Received wrong packet while waiting for a RESPONSE packet.\n");
            return(-1);
        }
    }
    else {
        printk("Timeout/failure while waiting for RESPONSE packet\n");

        /* Clear RX error/timeout events in the DW1000 status register. */
        dwt_write32bitreg(SYS_STATUS_ID,
                          SYS_STATUS_ALL_RX_TO | SYS_STATUS_ALL_RX_ERR);

        /* Reset RX to properly reinitialise LDE operation. */
        dwt_rxreset();
        return(0);
    }


    return(0);
}

double sd_resp_mode(uint16 *otherTagAddress)
{
    double distance;
    double tof;
    int poll_frame_nb=0;
    int poll_frame_nb_compare=0;
    uint16 currentInitID;
    uint16 currentRespID;
    uint16 srcAddress;
    uint16 destAddress;
    int packet_poll_request_nb;
    int currentMode=RESP_MODE;
    int packetKind;

    currentRespID=myID;
    currentInitID=0;

    /* Clear reception timeout to start next ranging process. */
    //dwt_setrxtimeout(POLL_RX_TIMEOUT_UUS);
    //dwt_setrxtimeout(0);

    /* Activate reception immediately. */
    dwt_rxenable(DWT_START_RX_IMMEDIATE);

    /// ricezione polling
    /* Poll for reception of a frame or error/timeout.
     * See NOTE 8 below.
     */
    while (!((status_reg = dwt_read32bitreg(SYS_STATUS_ID)) &
             (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_TO | SYS_STATUS_ALL_RX_ERR))&(OP_MODE==RESP_MODE))
    { };
    if(OP_MODE!=RESP_MODE)
    {
        // printk("Going back to main loop due to mode switching\n");
        /* Clear RX error/timeout events in the DW1000
         * status register.
         */
        dwt_write32bitreg(SYS_STATUS_ID,
                          SYS_STATUS_ALL_RX_TO | SYS_STATUS_ALL_RX_ERR);

        /* Reset RX to properly reinitialise LDE operation. */
        dwt_rxreset();
        return(0);
    }

    if (status_reg & SYS_STATUS_RXFCG) {
        //printk("Packet received\n");
        uint32 frame_len;

        /* Clear good RX frame event in the DW1000 status register. */
        dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_RXFCG);

        /* A frame has been received, read it into the local buffer. */
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;
        if (frame_len <= RX_BUFFER_LEN) {
           /// lettura effettiva del pacchetto ricevuto
           dwt_readrxdata(rx_buffer, frame_len, 0);
        }

        /* Check that the frame is a POLL frame sent by "DS TWR initiator"
         * example.
         * As the sequence number field of the frame is not relevant,
         * it is cleared to simplify the validation of the frame.
         */
        recPacketProcess(rx_buffer, &packetKind, &srcAddress, &destAddress, &packet_poll_request_nb);
        int checkResult=recPacketCheck(currentMode, currentInitID, currentRespID,
          poll_frame_nb, packetKind, srcAddress, destAddress, packet_poll_request_nb);

        /* Retrieve frame sequence number, only used if reception is correct */
        memcpy(&frame_seq_nb_rx, &rx_buffer[2], 1);

        poll_frame_nb=rx_buffer[ALL_MSG_SN_IDX];
        rx_buffer[ALL_MSG_SN_IDX] = 0;

        if(checkResult) {
            currentInitID=srcAddress;
            /*First, let us decide if the tag will enter the race to send a response packet.*/
            /// @Generoso: added if(myID >= 65000) { } else {} condition.
            /// @Generoso: else is the original form
            if(myID < 65000) {
              double pickUpValue= ((double) (sys_rand32_get()%100001))/100000;
              if (pickUpValue<=PICK_UP_PROB)
              {
                  //printk("Not entering the race, back to main loop\n");
                  return(0);
              }
            }

            uint32 resp_tx_time;
            int ret;
            //printk("Packet is from the initiator, good\n");
            /* Retrieve poll reception timestamp. */
            poll_rx_ts = get_rx_timestamp_u64();

            /*We now generate a random delay (in UUS) before sending the RESPONSE packet*/

            int num = (((uint32)sys_rand32_get()) % (MAX_RESPONSE_BACKOFF_DELAY_US - MIN_RESPONSE_BACKOFF_DELAY_US + 1)) + MIN_RESPONSE_BACKOFF_DELAY_US;
            //k_timer_start(&resp_TO_timer, K_MSEC(num*UUS_TO_DWT_TIME/1000), K_NO_WAIT);
            k_timer_start(&resp_TO_timer, K_MSEC((int)ROUND_UP(num/1000,1)), K_NO_WAIT);
            //printk("Back off delay: %d\n",num);
            //printk("Back off delay (rounded uus): %d\n",(int)ROUND_UP(num*512/499.2,1));
            /* Set send time for response in case the tag wins the backoff race*/
            resp_tx_time = (poll_rx_ts +
                            ((((int)ROUND_UP(num*512/499.2,1))+POLL_RX_TO_RESP_TX_DLY_UUS) * UUS_TO_DWT_TIME)) >> 8;
            dwt_setdelayedtrxtime(resp_tx_time);


            /* Write all timestamps in the final message.
             * See NOTE 11 below.
             */
            //response_msg_set_ts(&tx_resp_msg[RESPONSE_MSG_POLL_RX_TS_IDX], poll_rx_ts);
            //response_msg_set_ts(&tx_resp_msg[RESPONSE_MSG_RESP_TX_TS_IDX], resp_tx_ts);
            //printk("Timestamps written in the response packet\n");

            /* Prepare the response message. See NOTE 10 below.*/
            tx_resp_msg[ALL_MSG_SN_IDX] = poll_frame_nb;

            msg_set_src_address(tx_resp_msg, myID);
            msg_set_dest_address(tx_resp_msg, currentInitID);

            /* Zero offset in TX buffer. */
            dwt_writetxdata(sizeof(tx_resp_msg), tx_resp_msg, 0);

            /* Zero offset in TX buffer, ranging. */
            dwt_writetxfctrl(sizeof(tx_resp_msg), 0, 1);

            /* Poll for reception of a RESPONSE packet to the same request by another responder.*/
            while ((!((status_reg = dwt_read32bitreg(SYS_STATUS_ID)) &
                      (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR)))&(RESP_TO_TIMEOUT==0))
            {
                if (status_reg & SYS_STATUS_RXFCG) {
                    //printk("Packet received\n");
                    uint32 frame_len;

                    /* Clear good RX frame event in the DW1000 status register. */
                    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_RXFCG);

                    /* A frame has been received, read it into the local buffer. */
                    frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;
                    if (frame_len <= RX_BUFFER_LEN) {
                        dwt_readrxdata(rx_buffer, frame_len, 0);
                    }

                    /* Check whether the frame is a RESPONSE frame sent by another responder
                     * to the same ranging request */
                    recPacketProcess(rx_buffer, &packetKind, &srcAddress, &destAddress, &packet_poll_request_nb);
                    int checkResult=recPacketCheck(currentMode, currentInitID, currentRespID, poll_frame_nb, packetKind, srcAddress, destAddress, packet_poll_request_nb);

                    /* Retrieve poll request number, only used if reception is correct */
                    memcpy(&poll_frame_nb_compare, &rx_buffer[2], 1);

                    rx_buffer[ALL_MSG_SN_IDX] = 0;

                    //if ((memcmp(rx_buffer, rx_resp_msg, ALL_MSG_COMMON_LEN) == 0) && (poll_frame_nb_compare==poll_frame_nb)) {
                    if(checkResult) {
                        // if(checkResult)
                        //                         {
                        //                             printk("Check ok\n");
                        //                         }
                        //                         else
                        //                         {
                        //                             printk("Check not ok\n");
                        //                         }


                        //printk("Another responder won the race, back to main loop\n");
                        return(0);
                    }
                }

            }

            if(!RESP_TO_TIMEOUT)
            {
                //printk("RX error while in back off, back to main loop\n");
                return(-1);
            }
            else
            {
                //printk("Race won\n");
                RESP_TO_TIMEOUT=0;
            }
            /*The tag won the backoff race, let's send the packet*/

            /* Set expected delay and timeout for final message reception.
             */
            dwt_setrxaftertxdelay(RESP_TX_TO_FINAL_RX_DLY_UUS);
            dwt_setrxtimeout(10000);

            ret = dwt_starttx(DWT_START_TX_DELAYED | DWT_RESPONSE_EXPECTED);

            /* If dwt_starttx() returns an error, abandon this ranging
             * exchange and proceed to the next one. See NOTE 11 below.
             */
            if (ret == DWT_ERROR) {
                printk("RESPONSE message sending error - tx failed: %08lx\n", status_reg);
                return(-1);
            }

            /* Poll for reception of expected FINAL frame or error/timeout.
             * See NOTE 8 below.
             */
            while (!((status_reg = dwt_read32bitreg(SYS_STATUS_ID)) &
                     (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_TO | SYS_STATUS_ALL_RX_ERR)))
            { };


            if (status_reg & SYS_STATUS_RXFCG) {
                //printk("Final packet ok\n");
                /* Clear good RX frame event and TX frame sent in
                 * the DW1000 status register.
                 */
                dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_RXFCG | SYS_STATUS_TXFRS);

                /* A frame has been received, read it into the
                 * local buffer.
                 */
                frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFLEN_MASK;
                if (frame_len <= RX_BUF_LEN) {
                    dwt_readrxdata(rx_buffer, frame_len, 0);
                }

                /* Check that the frame is a final message sent by "DS TWR
                 * initiator" example.
                 * As the sequence number field of the frame is not used
                 * in this example, it can be zeroed to ease the validation
                 * of the frame.
                 */
                recPacketProcess(rx_buffer, &packetKind, &srcAddress, &destAddress, &packet_poll_request_nb);
                int checkResult=recPacketCheck(currentMode, currentInitID, currentRespID, poll_frame_nb, packetKind, srcAddress, destAddress, packet_poll_request_nb);


                rx_buffer[ALL_MSG_SN_IDX] = 0;
                if(checkResult) {
                    uint32 poll_tx_ts, resp_rx_ts, final_tx_ts;
                    uint32 poll_rx_ts_32, resp_tx_ts_32, final_rx_ts_32;
                    double Ra, Rb, Da, Db;
                    int64 tof_dtu;

                    /* Retrieve response transmission and final reception
                     * timestamps.
                     */
                    resp_tx_ts = get_tx_timestamp_u64();
                    final_rx_ts = get_rx_timestamp_u64();

                    /* Get timestamps embedded in the final message. */
                    final_msg_get_ts(&rx_buffer[FINAL_MSG_POLL_TX_TS_IDX], &poll_tx_ts);
                    final_msg_get_ts(&rx_buffer[FINAL_MSG_RESP_RX_TS_IDX], &resp_rx_ts);
                    final_msg_get_ts(&rx_buffer[FINAL_MSG_FINAL_TX_TS_IDX], &final_tx_ts);

                    /* Compute time of flight. 32-bit subtractions give
                     * correct answers even if clock has wrapped.
                     * See NOTE 12 below.
                     */
                    poll_rx_ts_32 = (uint32)poll_rx_ts;
                    resp_tx_ts_32 = (uint32)resp_tx_ts;
                    final_rx_ts_32 = (uint32)final_rx_ts;

                    Ra = (double)(resp_rx_ts - poll_tx_ts);
                    Rb = (double)(final_rx_ts_32 - resp_tx_ts_32);
                    Da = (double)(final_tx_ts - resp_rx_ts);
                    Db = (double)(resp_tx_ts_32 - poll_rx_ts_32);

                    tof_dtu = (int64)((Ra * Rb - Da * Db) / (Ra + Rb + Da + Db));

                    tof = tof_dtu * DWT_TIME_UNITS;
                    distance = tof * SPEED_OF_LIGHT;
                    tof_static=tof;
                    distance_static=distance;
                    /* Display computed distance on console. */
                    //sprintf(dist_str, "dist (%u): %3.2f m\n",
                    //        frame_seq_nb_rx, (float)(distance));
                    //printk("%s", dist_str);

                    /*LDN: added here code to send a CONFIRM packet RESP->INIT,
                     including the estimated distance. */
                    if(confirmationMessageOn)
                    {
                        // printk("sening confirmation message");
                        uint32 dist_int=(uint32) (distance*1000);
                        conf_msg_set_distance(&tx_conf_msg[CONF_MSG_DIST_IDX], dist_int);
                        /* Zero offset in TX buffer. */
                        tx_conf_msg[ALL_MSG_SN_IDX]=poll_frame_nb;
                        msg_set_src_address(tx_conf_msg, myID);
                        msg_set_dest_address(tx_conf_msg, currentInitID);
                        dwt_writetxdata(sizeof(tx_conf_msg), tx_conf_msg, 0);

                        /* Zero offset in TX buffer, ranging. */
                        dwt_writetxfctrl(sizeof(tx_conf_msg), 0, 1);
                        //dwt_setdelayedtrxtime((2000 * UUS_TO_DWT_TIME));
                        ret = dwt_starttx(DWT_START_TX_IMMEDIATE);
                        if (ret == DWT_SUCCESS) {
                            /* Poll DW1000 until TX frame sent event set.
                             * See NOTE 9 below.
                             */
                            while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                            { };

                            /* Clear TXFRS event. */
                            dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);

                            //printk("CONFIRM message sent (%u)\n", poll_frame_nb);

                            /* Increment frame sequence number after transmission of
                             * the final message (modulo 256).
                             */
                            frame_seq_nb++;

                        }
                        else {
                            printk("CONFIRM message sending error - tx failed: %08lx (%u)\n", status_reg, poll_frame_nb);
                            return(-1);
                        }
                    }
                    *otherTagAddress=currentInitID;
                    return(distance);
                }
                else {
                    printk("Received wrong packet while waiting for a FINAL packet (%u).\n", poll_frame_nb);
                    return(-1);
                }
            }
            else {
                printk("Packet reception failed/timed out while waiting for a FINAL packet (%u).\n", poll_frame_nb);

                /* Clear RX error/timeout events in the DW1000
                 * status register.
                 */
                dwt_write32bitreg(SYS_STATUS_ID,
                                  SYS_STATUS_ALL_RX_TO | SYS_STATUS_ALL_RX_ERR);

                /* Reset RX to properly reinitialise LDE operation. */
                dwt_rxreset();
                return(-1);
            }
        }
        else {
            printk("Received wrong packet while waiting for a POLL packet.\n");
            return(-1);
        }
    }
    else {
        printk("Timeout/failure while waiting for POLL packet\n");

        /* Clear RX error/timeout events in the DW1000
         * status register.
         */
        dwt_write32bitreg(SYS_STATUS_ID,
                          SYS_STATUS_ALL_RX_TO | SYS_STATUS_ALL_RX_ERR);

        /* Reset RX to properly reinitialise LDE operation. */
        dwt_rxreset();
        return(-1);
    }

    /// Note:
    /// serve questo return?
    return(0);
}

static void dwt_isr_helper(void)
{
    process_isr = 1;
}

/*federico*/
/*inizializzazione struttura pacchetto advertisemnt*/
/*static u8_t mfg_data[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

 static const struct bt_data ad[] = {*/
/*BT_DATA(BT_DATA_MANUFACTURER_DATA, mfg_data, 3),*/
/*    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),

 BT_DATA(BT_DATA_SVC_DATA128, mfg_data, 16)
 };
 */
int err1;
/*fine federico*/



/// notifica la distanza rilevata con un tag
void ble_notify_distance(uint16_t otherTagAddress, float distance) {

  /* BLE Configuration */
  // ble_reps_t * ble_reps;
  // uint8_t      ble_buf [120] = {0};
  //
  // ble_reps = (ble_reps_t *)(&ble_buf[0]);
  //
  // k_yield();
  //
  // if(is_connected()) {
  //   /*We send up a BLE notification if the minimum distance threshold is violated*/
  //   ble_reps->cnt = 1;
  //   ble_reps->ble_rep[0].my_id = myID;
  //   ble_reps->ble_rep[0].node_id = otherTagAddress;
  //   ble_reps->ble_rep[0].dist = distance;
  //   ble_reps->ble_rep[0].tqf = 0;
  //
  //   dwm1001_notify((uint8_t*)ble_buf, 1 + sizeof(ble_rep_t) * ble_reps->cnt);
  //
  //    printk("Notified");
  // } else {
  //   printk("Not connected, avoiding notify");
  // }
}



/*! --------------------------------------------------------------------------
 * @fn main()
 *
 * @brief Application entry point.
 *
 * @param  none
 *
 * @return none
 */
int dw_main(void)
{
    int ret;
    /* Display application name on console. */
    printk(APP_HEADER);
    printk(APP_NAME);
    printk(APP_VERSION);
    printk(APP_LINE);



    /* Install DW1000 IRQ handler. */
    /* Note: dwt_isr cannot be called in actual ISR because zephyr doesn't
     * like APIs like spi being used during the ISR
     */
    port_set_deca_isr(dwt_isr_helper);


    /* Configure DW1000 SPI */
    openspi();

    /* Reset and initialise DW1000.
     * For initialisation, DW1000 clocks must be temporarily set to
     * crystal speed. After initialisation SPI rate can be increased
     * for optimum performance.
     */
    /* Target specific drive of RSTn line into DW1000 low for a period. */
    reset_DW1000();

    port_set_dw1000_slowrate();
    if (dwt_initialise(DWT_LOADUCODE) == DWT_ERROR) {
        printk("INIT FAILED");
        k_sleep(K_MSEC(500));
        while (1)
        { };
    }
    port_set_dw1000_fastrate();

    dwt_enablegpioclocks();

    /*LED setup section*/
#ifndef BEAMDIGITAL_BOARD
    int flags_LED = (GPIO_OUTPUT);
#else
    int flags_LED = (GPIO_OUTPUT_LOW|GPIO_PULL_DOWN);
#endif
    /* Get GPIO device binding */
    gpiob = device_get_binding(GPIO_DRV_NAME);
    if (!gpiob) {
        printk("Cannot find %s!\n", GPIO_DRV_NAME);
        return -1;
    }
    else
    {
        printk("Binded to %s!\n", GPIO_DRV_NAME);

    }

    /* Setup GPIO output */
    ret = gpio_pin_configure(gpiob, GPIO_OUT_PIN_RED, flags_LED);
    if (ret) {
        printk("Error configuring " GPIO_DRV_NAME "%d!\n", GPIO_OUT_PIN_RED);
    }

    ret = gpio_pin_configure(gpiob, GPIO_OUT_PIN_GREEN, flags_LED);
    if (ret) {
        printk("Error configuring " GPIO_DRV_NAME "%d!\n", GPIO_OUT_PIN_GREEN);
    }

    ret = gpio_pin_configure(gpiob, GPIO_OUT_PIN_BLUE, flags_LED);
    if (ret) {
        printk("Error configuring " GPIO_DRV_NAME "%d!\n", GPIO_OUT_PIN_BLUE);
    }


#ifndef BEAMDIGITAL_BOARD
    ret = gpio_pin_configure(gpiob, GPIO_OUT_PIN_RED2, flags_LED);
    if (ret) {
        printk("Error configuring " GPIO_DRV_NAME "%d!\n", GPIO_OUT_PIN_RED2);
    }
#else

    ret = gpio_pin_configure(gpiob, GPIO_OUT_PIN_BUZZER, flags_LED);
    if (ret) {
        printk("Error configuring " GPIO_DRV_NAME "%d!\n", GPIO_OUT_PIN_BUZZER);
    }

#endif

    /* Configure DW1000. See NOTE 7 below. */
    dwt_configure(&config);

    /* Register RX call-back. */
    dwt_setcallbacks(&tx_conf_cb, &rx_ok_cb, &rx_to_cb, &rx_err_cb);

    /* Enable wanted interrupts (TX confirmation, RX good frames,
     * RX timeouts and RX errors).
     */
    dwt_setinterrupt(DWT_INT_TFRS | DWT_INT_RFCG | DWT_INT_RFTO |
                     DWT_INT_RXPTO | DWT_INT_RPHE | DWT_INT_RFCE |
                     DWT_INT_RFSL | DWT_INT_SFDT, 1);

    /* Apply default antenna delay value. See NOTE 1 below. */
    dwt_setrxantennadelay(RX_ANT_DLY);
    dwt_settxantennadelay(TX_ANT_DLY);

    //LDN: introduced timer for operation mode switch

    //k_timer_init(&my_timer, op_mode_switch, NULL);

    /// @Generoso: meglio questa soluzione. Non abbiamo timer che vanno e
    ///   vengono. Tuttavia c'è ancora qualcosa che non torna. Ci sono momenti
    ///   in cui la distanza è negativa non per un errore (-1) ma più
    ///

    /*LDN: the following call should get the tag ID from somewhere. Currently,
     * the function extracts a random int between 0 and 64999, with the remaining ones reserved as follows:
     - addresses 65000-65534: reserved for infrastructure tags;
     - 65535 reserved for broadcast packets.
     */
    //getMyID(&myID);
    myID=65000;
    printk("ID: %d -> 0x%2x\n", myID, myID);
    OP_MODE=INIT_MODE;
    /// @Generoso: for UWBI Tag only, responder mode only
    if(myID >= 65000) {
      OP_MODE=RESP_MODE;
      // printk("UWBI Tag found. Forcing responder mode\n");
    } else {
      /// @Generoso: moved into the else branch of the condition

      //LDN: timer for periodic reading of BLE commands
      k_timer_init(&BLE_command_timer, BLE_command_timer_handler, NULL);

      //LDN: timer for periodic link keep BLE advertisement packet
      k_timer_init(&link_keep_timer, link_keep_timer_handler, NULL);

      //LDN: timer for stopping link keep BLE advertisement when started
      k_timer_init(&link_keep_ad_timer, link_keep_ad_timer_handler, NULL);

      //LDN: timer for stopping violation BLE advertisement when started
      k_timer_init(&violation_ad_timer, violation_ad_timer_handler, NULL);
    }

    k_timer_init(&my_timer, my_timer_handler, NULL);

    /* Timer for transmission of RESPONSE packet by responder*/
    k_timer_init(&resp_TO_timer, resp_TO_timer_handler, NULL);

    /* Timer for automatic turning off of leds*/
    k_timer_init(&leds_off_timer, leds_off_timer_handler, NULL);

    /* Timer for tautomatic reset*/
    k_timer_init(&auto_reset_timer, auto_reset_timer_handler, NULL);

    /* Timer for automatic turning off of leds*/
    k_timer_init(&led_lk_off_timer, led_lk_timer_handler, NULL);

#ifdef BEAMDIGITAL_BOARD
    k_timer_init(&buzzer_off_timer, buzzer_off_timer_handler, NULL);
#endif

    k_timer_init(&SOS_ad_timer, SOS_ad_timer_handler, NULL);
    /* Init Button Interrupt */
    int flags = (GPIO_INPUT      |
                 GPIO_PULL_UP    |
                 GPIO_INT_EDGE   |
                 GPIO_INT_EDGE_BOTH);

    gpio_pin_configure(gpiob, GPIO_IN_PIN_BUTTON, flags);

    gpio_pin_interrupt_configure(gpiob, GPIO_IN_PIN_BUTTON, GPIO_INT_EDGE_TO_ACTIVE|GPIO_INT_EDGE_TO_INACTIVE);

    gpio_init_callback(&gpio_cb, button_event, BIT(GPIO_IN_PIN_BUTTON));

    gpio_add_callback(gpiob, &gpio_cb);


    /* Intializes random number generator */
    //time_t t;
    //srand((unsigned) time(&t));
    /* Set preamble timeout for expected frames. See NOTE 6 below. */
    //NOTE: no use of preamble tmo's yet
    // dwt_setpreambledetecttimeout(PRE_TIMEOUT);

#ifndef BEAMDIGITAL_BOARD
    /* Configure DW1000 LEDs */
    dwt_setleds(1);
#endif
    k_yield();

    int tCounter;
    for (tCounter=0;tCounter<N_DETECTABLE_TAGS;tCounter=tCounter+1)
    {
        detectedTagIDs[tCounter]=0;
        violationTagFlag[tCounter]=0;
    }

    numDistances=0;
    averageDistance=0;
    /*Variable storing flag for accuracy tests: if configured to 1, the variable forces the chip to stay in the mode set above (either RESP_MODE or INIT_MODE) and, if the chip is set to RESP_MODE, averages the distance estimation over 100 measurements, before freezing the chip*/
    accuracyTests=0;
    if(accuracyTests==0)
    {
    /*The following block of code can be commented out if one wants to keep the device in one role->*/

     if(OP_MODE==INIT_MODE)
     {
     nextSwitchTime=(((uint32)sys_rand32_get()) % (MAX_INIT_PERIOD_MS - MIN_INIT_PERIOD_MS + 1)) + MIN_INIT_PERIOD_MS;
     }
     else
     {
     nextSwitchTime=(((uint32)sys_rand32_get()) % (MAX_RESP_PERIOD_MS - MIN_RESP_PERIOD_MS + 1)) + MIN_RESP_PERIOD_MS;
     }
     k_timer_start(&my_timer, K_MSEC(nextSwitchTime), K_NO_WAIT);

         /*->until here*/
    }

    k_timer_start(&BLE_command_timer, K_MSEC(BLE_COMMAND_READ_PERIOD_MSEC), K_NO_WAIT);
    k_timer_start(&link_keep_timer, K_MSEC(LINK_KEEP_PERIOD_MSEC), K_NO_WAIT);
    int autoResetOn=1;
    if(autoResetOn)
    {
        k_timer_start(&auto_reset_timer, K_SECONDS(AUTO_RESET_PERIOD_SEC), K_NO_WAIT);
    }

    RESP_TO_TIMEOUT=0;
    led_green_off();
    led_blue_off();
    led_red1_off();
#ifndef BEAMDIGITAL_BOARD
    led_red2_off();
#endif
    SOS_AD_ON=0;
    CONNECTION_REQUEST_ON=0;
    VIOLATION_AD_ON=0;
    BLE_LK_ON=0;
    VIOLATION_FLAG=0;
    LEDS_OFF_TENTATIVE=0;
    /*We stop BLE advertising to save battery, it will be turned on when needed:
     1) when a distance violation is detected and should be reported
     2) when a periodic link keep packet should be sent
     3) when a button press is detected, and the device must enter in configuration mode
     */

    /// @Generoso: commented
    // ble_stop_advertising();
    /* Loop forever alternating between sending and responding to ranging requests. */
    while (1) {

        if(sleepingRequested||sleepingOn)
        {
            if(sleepingRequested)
            {
                sleepingOn=1;
                sleepingRequested=0;
                //dwt_configuresleep(DWT_PRESRV_SLEEP | DWT_CONFIG, DWT_WAKE_CS | DWT_SLP_EN);
                //dwt_entersleep();
                led_red1_off();
                #ifdef BEAMDIGITAL_BOARD
		            buzzer_off();
        	    #endif

                printk("DWT1000 in sleeping mode\n");
            }
            else
            {
                __SEV();
                __WFE();
                __WFE();
            }
        }
        else
        {
          double distance_int=0;
          uint16 otherTagAddress;
          switch (OP_MODE) {
              case INIT_MODE:
                  printk("Role: initiator\n");
                  /* Set expected response's delay and timeout. See NOTE 4, 5 and 6 below.
                   */
                  /* Configure DW1000. This is a ugly shortcut to solve the problem that
                   the DW1000 would not resume normal transmission after a switch from
                   RESP_MODE to INIT_MODE*/
                  dwt_configure(&config);
                  dwt_setrxaftertxdelay(POLL_TX_TO_RESP_RX_DLY_UUS);
                  dwt_setrxtimeout(RESP_RX_TIMEOUT_UUS);
                  distance_int=sd_init_mode(&otherTagAddress);

                  /* Execute a delay between ranging exchanges. */
                  Sleep(RNG_DELAY_MS);
                  break;

              case RESP_MODE:
                  printk("Role: responder\n");
                  //dwt_setrxaftertxdelay(RESP_TX_TO_FINAL_RX_DLY_UUS);
                  dwt_setrxtimeout(0);
                  distance_int=sd_resp_mode(&otherTagAddress);
                  break;

              default:
                  break;
          }

          /// Note:
          /// sd_resp_mode: quando scade il timeout dovrebbe ritornare 0 e
          /// stampare a video 'Timeout ...' tuttavia va nel branch d < 0.
          /// non cabia il risultato, però
          if(distance_int>0)
          {
              char outStr[128];
              char outStr2[128];
              sprintf(outStr, "Distance : %f m\n",distance_int);
              printk("%s",outStr);

              /* If the chip is in RESP_MODE and the variable accuracyTests is
              set to 1 the first 100 measurements are used to obtain an average
              distance estimation, and then the chip freezes.
              Note that this should be done with only one other device
              around set to INIT_MODE and accuracyTests set to 1, to ensure that
              the 100 measurements are taken with the same device;
              otherwise the result will be meaningless */
              if((OP_MODE==RESP_MODE)&&(accuracyTests==1))
              {
                  averageDistance=(averageDistance*numDistances+distance_int)/(numDistances+1);
                  numDistances=numDistances+1;
                  printk("Distance estimate #%d\n",numDistances);
                  sprintf(outStr, "Average distance: %f \n",averageDistance);
                  printk("%s",outStr);
                  if(numDistances==100)
                  {
                      k_sleep(K_MSEC(500));
                      while (1)
                      { };
                  }

              }

              /// @Generoso: added && myID < 65000
              /// qui dentro praticamente fa tutte le cose che devono essere fatte
              /// per gestire le violazioni. Il Tag UWBI non dovrebbe fare, ma
              /// si dovrebbe occupare solo di rispondere a tutte le richieste che
              /// gli arrivano
              if(distance_int<SAFETY_DISTANCE_THRESHOLD && myID < 65000)
              {
                  VIOLATION_FLAG=1;
                  LEDS_OFF_TENTATIVE=0;
                  k_timer_start(&leds_off_timer, K_MSEC(LEDS_OFF_TENTATIVE_MSEC),K_NO_WAIT);
  #ifdef BEAMDIGITAL_BOARD
                  printk("Buzzer on \n");
                  k_timer_start(&buzzer_off_timer, K_MSEC(BUZZER_OFF_MSEC), K_NO_WAIT);
  #endif
                  numViolations=numViolations+1;
                  sprintf(outStr2, "Too close to tag %d, minimum distance is %f m!\n", otherTagAddress, (double) SAFETY_DISTANCE_THRESHOLD);
                  printk("%s",outStr2);
                  led_green_on();
                  led_blue_on();
                  led_red1_on();
  #ifndef BEAMDIGITAL_BOARD
                  led_red2_on();
  #else
                  buzzer_on();
  #endif
                  /*The code below scans the detectedTagIDs and violationTagFlag vectors searching for the slot currently occupied by the
                   other tag or for an empty slot*/
                  tCounter=0;
                  int firstAvailableSlot=-1;
                  while ((detectedTagIDs[tCounter]>0) && (tCounter<N_DETECTABLE_TAGS))/*We stop as soon as we find an unused slot, or when the  whole vector was scanned*/
                  {
                      if (otherTagAddress==detectedTagIDs[tCounter])/*We found the slot occupied by the other tag, let's exit he loop to update its status*/
                      {
                          printk("ID found in slot %d\n",tCounter);
                          break;
                      }
                      else
                      {
                          if((violationTagFlag[tCounter]==0)&&(firstAvailableSlot==-1)) /*This is the first slot occupied by a tag that was above threshold in the last measurement, so it will be used if we don't find neither the tag ID nor an unused slot in the array */
                          {
                              printk("First available slot found: %d\n",tCounter);
                              firstAvailableSlot=tCounter;
                          }
                          tCounter=tCounter+1;
                      }
                  }
                  if (tCounter<N_DETECTABLE_TAGS)/*We left the while prematurely, either we found the slot occupied by the other tag or an unused slot, let's use it (if it is the slot occupied by the other tag we would not actually need to update the Tag ID, but we save the if to check for it)*/
                  {
                      printk("Updating info for slot %d\n",tCounter);
                      detectedTagIDs[tCounter]=otherTagAddress;
                      if(violationTagFlag[tCounter]<MAX_VIOLATION_COUNT)
                          violationTagFlag[tCounter]=violationTagFlag[tCounter]+1;
                      printk("Content of slot %d: ID=%d, violations=%d\n",tCounter,detectedTagIDs[tCounter],violationTagFlag[tCounter]);

                  }
                  else/*We scanned the whole vector, so all slots are used. Let's see if we found a slot we can reuse*/
                  {
                      if(firstAvailableSlot>=0)/*Yes: let's reuse it*/
                      {
                          printk("Reusing first available slot, %d\n",firstAvailableSlot);
                          detectedTagIDs[firstAvailableSlot]=otherTagAddress;
                          violationTagFlag[firstAvailableSlot]=1;
                          printk("Content of slot %d: ID=%d, violations=%d\n",tCounter,detectedTagIDs[firstAvailableSlot],violationTagFlag[firstAvailableSlot]);
                      }
                      else/*No: all slots are used for IDs currently below threshold, let's overwrite one of them. We pick slot 0, but we could also pick ome randomly.*/
                      {
                          printk("No available slot, overwriting slot 0\n");
                          detectedTagIDs[0]=otherTagAddress;
                          violationTagFlag[0]=1;
                          printk("Content of slot 0: ID=%d, violations=%d\n",detectedTagIDs[0],violationTagFlag[0]);

                      }
                  }

                  /*We send up a BLE notification if the minimum distance threshold is violated*/
                  /*ble_reps->cnt = 1;
                   ble_reps->ble_rep[0].node_id = otherTagAddress;
                   ble_reps->ble_rep[0].dist = (float)(distance_int);
                   ble_reps->ble_rep[0].tqf = 0;*/

                  /*dwm1001_notify((uint8_t*)ble_buf,
                   1 + sizeof(ble_rep_t) * ble_reps->cnt);*/
                  /// @Generoso: commented?
                  //ble_stop_advertising();
                  /*federico*/

                  /// @Generoso: commented from line 2213 to line 2262
                  /// @Generoso: added VIOLATION_AD_ON=1
                  ble_notify_distance(otherTagAddress, (float)(distance_int));
                  VIOLATION_AD_ON=1;
                  //array con identificatore della scheda
                  // uint8_t splittedOTA[2];
                  // splittedOTA[0]=otherTagAddress & 0xff;
                  // splittedOTA[1]=(otherTagAddress >> 8);
                  //
                  // uint8_t splittedMTA[2];
                  // splittedMTA[0]=myID & 0xff;
                  // splittedMTA[1]=(myID >> 8);
                  //
                  // //array con la distanza rilevata in ASCII
                  // char c[10];
                  // sprintf(c , "%lf" , distance_int);

                  // //pacchetto advertisement, indirizzo + distanza in ASCII
                  // mfg_data[0]=splittedMTA[0]; // @generoso
                  // mfg_data[1]=splittedMTA[1]; // @generoso
                  // mfg_data[2]=c[0];
                  // mfg_data[3]=c[1];
                  // mfg_data[4]=c[2];
                  // mfg_data[5]=c[3];
                  // mfg_data[6]=c[4];
                  // mfg_data[7]=c[5];
                  // mfg_data[8]=c[6];
                  // mfg_data[9]=c[7];
                  // mfg_data[10]=splittedOTA[0]; // @generoso
                  // mfg_data[11]=splittedOTA[1]; // @generoso

                  // @generoso questa funzione ritorna errore se è già stato avviato l'advertising
                  // int err2 = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), NULL, 0);
                  //
                  // //printk("advstart %d\n",err2);
                  // if (err2 && err2 != -69) { // @generoso -69 == -EALREADY
                  //     printk("Violation advertising failed to start (err %d)\n", err2);
                  // }
                  // else
                  // {
                  //     err1 = bt_le_adv_update_data(ad, ARRAY_SIZE(ad),
                  //                                  NULL, 0);
                  //     //To be checked: the update always fails, returning -11
                  //     if(err1)
                  //     {
                  //         printk("Advertising failed to update (err %d)\n", err1);
                  //     }
                  //     else
                  //     {
                  //         VIOLATION_AD_ON=1;
                  //         printk("Violation advertising started\n");
                  //         k_timer_start(&violation_ad_timer, K_MSEC(VIOLATION_AD_DURATION_MSEC), K_NO_WAIT);
                  //     }
                  // }
                  /*fine federico*/

                  /*Place holder per funzione di immagazzinamento violazione in log, non ancora implementato.
                   Possibilità:
                   1) uso di sistema di log di zephyr, definendo un custom backend che immagazzini l'entry sulla memoria flash, (da verificare però come implementarlo) oppure in RAM, usando k_malloc; questo permetterebbe di usare il sistema di time stamping di zephyr
                   2) allocando uno stack in RAM usando direttamente k_malloc, e scrivendo le funzioni per leggere, scrivere e gestire lo stack.
                   */
                  //storeViolationInLog(splittedOTA,distance_int,numViolations)

              }
              else
              {
                  /*modifica al print per vedere nodo con cui scambia oltre distanza minima FEDERICO*/
                  sprintf(outStr2, "Minimum distance of %f m met with tag %d \n",(double) SAFETY_DISTANCE_THRESHOLD, otherTagAddress);
                  printk("%s",outStr2);
                  tCounter=0;
                  int totalViolations=0;
                  for (tCounter=0;tCounter<N_DETECTABLE_TAGS;tCounter=tCounter+1) /*We scan the whole vector to check how many violations are there*/
                  {
                      if(otherTagAddress==detectedTagIDs[tCounter])/*If the tag is already in the vector, we set its violation flag to 0, and remove it*/
                      {
                          printk("ID %d found in slot %d, updating it\n", otherTagAddress,tCounter);
                          if(violationTagFlag[tCounter]>1)
                              violationTagFlag[tCounter]=violationTagFlag[tCounter]-1;
                          else
                          {
                              violationTagFlag[tCounter]=0;
                              detectedTagIDs[tCounter]=0;
                          }
                          printk("Content of slot %d: ID=%d, violations=%d\n",tCounter,detectedTagIDs[tCounter],violationTagFlag[tCounter]);
                      }
                      totalViolations=totalViolations+violationTagFlag[tCounter];

                  }
                  /*Let's check if there are no violations left; if this is the case, let's turn off the LEDS*/
                  if(totalViolations==0)
                  {
                      led_green_off();
                      led_blue_off();
                      led_red1_off();
  #ifndef BEAMDIGITAL_BOARD
                      led_red2_off();
  #endif
                      VIOLATION_FLAG=0;
                  }
                  printk("Total violations = %d\n", totalViolations);
              }


          }
          else {
              if(distance_int==0) {
                  printk("Timeout during ranging procedure\n");
              } else {
                char outStr[128];
                sprintf(outStr, "Distance : %f m\n",distance_int);
                printk("%s", outStr);
                printk("Error during ranging procedure\n");
              }
          }
        }
      }
}


static void conf_msg_get_distance(uint8 *ts_field, uint32 *dist)
{
    int i;
    *dist = 0;
    for (i = 0; i < CONF_MSG_DIST_LEN; i++)
    {
        *dist += ts_field[i] << (i * 8);
    }
}

static void conf_msg_set_distance(uint8 *ts_field, const uint32 dist)
{
    int i;
    for (i = 0; i < CONF_MSG_DIST_LEN; i++)
    {
        ts_field[i] = (dist >> (i * 8)) & 0xFF;
    }
}

static void msg_set_src_address(uint8 *ts_field, const uint16 address)
{
    int i;
    for (i = 0; i < ALL_MSG_ADDRESS_LEN; i++)
    {
        ts_field[i+ALL_MSG_SOURCE_ADDRESS_IDX] = (address >> (i * 8)) & 0xFF;
    }
}

static void msg_set_dest_address(uint8 *ts_field, const uint16 address)
{
    int i;
    for (i = 0; i < ALL_MSG_ADDRESS_LEN; i++)
    {
        ts_field[i+ALL_MSG_DEST_ADDRESS_IDX] = (address >> (i * 8)) & 0xFF;
    }
}

/*! --------------------------------------------------------------------------
 * @fn get_tx_timestamp_u64()
 *
 * @brief Get the TX time-stamp in a 64-bit variable.
 *        /!\ This function assumes that length of time-stamps is 40 bits,
 *            for both TX and RX!
 *
 * @param  none
 *
 * @return  64-bit value of the read time-stamp.
 */
static uint64 get_tx_timestamp_u64(void)
{
    uint8 ts_tab[5];
    uint64 ts = 0;

    dwt_readtxtimestamp(ts_tab);
    for (int i = 4; i >= 0; i--) {
        ts <<= 8;
        ts |= ts_tab[i];
    }
    return ts;
}

/*! --------------------------------------------------------------------------
 * @fn get_rx_timestamp_u64()
 *
 * @brief Get the RX time-stamp in a 64-bit variable.
 *        /!\ This function assumes that length of time-stamps is 40 bits,
 *            for both TX and RX!
 *
 * @param  none
 *
 * @return  64-bit value of the read time-stamp.
 */
static uint64 get_rx_timestamp_u64(void)
{
    uint8 ts_tab[5];
    uint64 ts = 0;

    dwt_readrxtimestamp(ts_tab);
    for (int i = 4; i >= 0; i--) {
        ts <<= 8;
        ts |= ts_tab[i];
    }
    return ts;
}

/*! --------------------------------------------------------------------------
 * @fn final_msg_get_ts()
 *
 * @brief Read a given timestamp value from the final message.
 *        In the timestamp fields of the final message, the least
 *        significant byte is at the lower address.
 *
 * @param  ts_field  pointer on the first byte of the timestamp field
 *         to read ts  timestamp value
 *
 * @return none
 */
static void final_msg_get_ts(const uint8 * ts_field, uint32 * ts)
{
    *ts = 0;
    for (int i = 0; i < FINAL_MSG_TS_LEN; i++) {
        *ts += ts_field[i] << (i * 8);
    }
}


/*! --------------------------------------------------------------------------
 * @fn final_msg_set_ts()
 *
 * @brief Fill a given timestamp field in the final message with the
 *        given value. In the timestamp fields of the final
 *        message, the least significant byte is at the lower address.
 *
 * @param  ts_field  pointer on the first byte of the timestamp field to fill
 *         ts  timestamp value
 *
 * @return none
 */
static void final_msg_set_ts(uint8 *ts_field, uint64 ts)
{
    for (int i = 0; i < FINAL_MSG_TS_LEN; i++) {
        ts_field[i] = (uint8) ts;
        ts >>= 8;
    }
}

/*! --------------------------------------------------------------------------
 * @fn response_msg_get_ts()
 *
 * @brief Read a given timestamp value from the response message.
 *        In the timestamp fields of the final message, the least
 *        significant byte is at the lower address.
 *
 * @param  ts_field  pointer on the first byte of the timestamp field
 *         to read ts  timestamp value
 *
 * @return none
 */
/*
 static void response_msg_get_ts(const uint8 * ts_field, uint32 * ts)
 {
 *ts = 0;
 for (int i = 0; i < RESPONSE_MSG_TS_LEN; i++) {
 *ts += ts_field[i] << (i * 8);
 }
 }
 */

/*! --------------------------------------------------------------------------
 * @fn response_msg_set_ts()
 *
 * @brief Fill a given timestamp field in the response message with the
 *        given value. In the timestamp fields of the final
 *        message, the least significant byte is at the lower address.
 *
 * @param  ts_field  pointer on the first byte of the timestamp field to fill
 *         ts  timestamp value
 *
 * @return none
 */
/*
 static void response_msg_set_ts(uint8 *ts_field, uint64 ts)
 {
 for (int i = 0; i < RESPONSE_MSG_TS_LEN; i++) {
 ts_field[i] = (uint8) ts;
 ts >>= 8;
 }
 }
 */

/*! --------------------------------------------------------------------------
 * @fn rx_ok_cb()
 *
 * @brief Callback to process RX good frame events
 *
 * @param  cb_data  callback data
 *
 * @return  none
 */
static void rx_ok_cb(const dwt_cb_data_t * cb_data)
{

    /* Clear local RX buffer to avoid having leftovers from previous receptions.
     * This is not necessary but is included here to aid reading the RX
     * buffer.
     */
    for (int i = 0 ; i < RX_BUF_LEN; i++ ) {
        rx_buffer[i] = 0;
    }

    /* A frame has been received, copy it to our local buffer. */
    if (cb_data->datalength <= RX_BUF_LEN) {
        dwt_readrxdata(rx_buffer, cb_data->datalength, 0);
    }

    /* Set corresponding inter-frame delay. */
    tx_delay_ms = DFLT_TX_DELAY_MS;

    /* TESTING BREAKPOINT LOCATION #1 */
}

/*! --------------------------------------------------------------------------
 * @fn rx_to_cb()
 *
 * @brief Callback to process RX timeout events
 *
 * @param  cb_data  callback data
 *
 * @return  none
 */
static void rx_to_cb(const dwt_cb_data_t * cb_data)
{
    /* Set corresponding inter-frame delay. */
    tx_delay_ms = RX_TO_TX_DELAY_MS;

    /* TESTING BREAKPOINT LOCATION #2 */
}

/*! --------------------------------------------------------------------------
 * @fn rx_err_cb()
 *
 * @brief Callback to process RX error events
 *
 * @param  cb_data  callback data
 *
 * @return  none
 */
static void rx_err_cb(const dwt_cb_data_t * cb_data)
{
    /* Set corresponding inter-frame delay. */
    tx_delay_ms = RX_ERR_TX_DELAY_MS;

    /* TESTING BREAKPOINT LOCATION #3 */
}

/*! --------------------------------------------------------------------------
 * @fn tx_conf_cb()
 *
 * @brief Callback to process TX confirmation events
 *
 * @param  cb_data  callback data
 *
 * @return  none
 */
static void tx_conf_cb(const dwt_cb_data_t * cb_data)
{
    /* This callback has been defined so that a breakpoint can be put here to
     * check it is correctly called but there is actually nothing specific to
     * do on transmission confirmation in this example. Typically, we could
     * activate reception for the response here but this is automatically
     * handled by DW1000 using DWT_RESPONSE_EXPECTED parameter when calling
     * dwt_starttx().
     * An actual application that would not need this callback could simply
     * not define it and set the corresponding field to NULL when calling
     * dwt_setcallbacks(). The ISR will not call it which will allow to save
     * some interrupt processing time.
     */
    printk("Interrupt for TX confirmation hit\n");
    /* TESTING BREAKPOINT LOCATION #4 */
}


/*! --------------------------------------------------------------------------
 * @fn recPacketProcess()
 *
 * @brief Function extracting information from a received packet
 *
 * @param  rx_buffer, &packet_kind, &packet_source, &packet_destination, &packet_poll_request_nb
 *
 * @return  none
 */


static void recPacketProcess(uint8 *rx_buffer, int *packet_kind, uint16 *packet_source, uint16 *packet_destination, int *packet_poll_request_nb)
{
    *packet_kind=rx_buffer[ALL_MSG_KIND_IDX];
    *packet_poll_request_nb=rx_buffer[ALL_MSG_SN_IDX];
    int i;
    *packet_source = 0;
    for (i = 0; i < ALL_MSG_ADDRESS_LEN; i++)
    {
        *packet_source += rx_buffer[i+ALL_MSG_SOURCE_ADDRESS_IDX] << (i * 8);
    }
    *packet_destination = 0;
    for (i = 0; i < ALL_MSG_ADDRESS_LEN; i++)
    {
        *packet_destination += rx_buffer[i+ALL_MSG_DEST_ADDRESS_IDX] << (i * 8);
    }
}

/*! --------------------------------------------------------------------------
 * @fn recPacketCheck()
 *
 * @brief Function checking whether a received packet meets specific characteristics
 *
 * @param  current_mode, currentInitID, currentRespID, current_poll_request_nb, packet_kind, packet_source, packet_destination, packet_poll_request_nb
 *
 * @return  1 if the packet is a match, 0 otherwise
 */
static int recPacketCheck(int current_mode, uint16 currentInitID, uint16 currentRespID, int current_poll_request_nb, int packet_kind, uint16 packet_source, uint16 packet_destination, int packet_poll_request_nb)
{
    /* This function checks whether a received packet meets a specific set of conditions on type,
     * source, destination, and poll request number.
     */
    switch(packet_kind)
    {
        case POLL_TYPE:
            return(1);// No additional check if it is a POLLING packet
            break;

        case RESP_TYPE:
            if((packet_destination==currentInitID)&&(packet_poll_request_nb==current_poll_request_nb))
                return(1); //Either I am an initiator, and this packet is for me, or I am a responder, and this packet is a response to the same polling request I am currently contending for.
            break;

        case FINAL_TYPE:
            if((current_mode==RESP_MODE)&&(packet_destination==currentRespID)&&(packet_poll_request_nb==current_poll_request_nb)&&(packet_source==currentInitID))
                return(1); //I am a responder, and this packet is the FINAL packet to the same polling request I am currently participating into.
            break;

        case CONF_TYPE:
            if((current_mode==INIT_MODE)&&(packet_destination==currentInitID)&&(packet_poll_request_nb==current_poll_request_nb)&&(packet_source==currentRespID))
                return(1); //I am an initiator, and this packet is the CONFIRMATION packet to the same polling request I am currently participating into.
            break;
    }

    return(0); //In all other cases, this packet is not a match for my current status.
}
/*! --------------------------------------------------------------------------
 * @fn getMyID()
 *
 * @brief Function to get the ID at boot time. Currently randomly generated.
 *
 * @param  myID, buffer
 *
 * @return  none
 */
static void getMyID(uint16 *tagID)
{
    *tagID = (uint16) sys_rand32_get()%65000;
    /*Addresses between 65000 and 65534 are reserved to infrastructure tags;
     address 65535 is reserved for broadcast.
     */
}


/*****************************************************************************
 * NOTES:
 *
 * 1. The sum of the values is the TX to RX antenna delay, experimentally
 *    determined by a calibration process. Here we use a hard coded typical
 *    value but, in a real application, each device should have its own
 *    antenna delay properly calibrated to get the best possible precision
 *    when performing range measurements.
 * 2. The messages here are similar to those used in the DecaRanging ARM
 *    application (shipped with EVK1000 kit). They comply with the IEEE
 *    802.15.4 standard MAC data frame encoding and they are following the
 *    ISO/IEC:24730-62:2013 standard. The messages used are:
 *     - a poll message sent by the initiator to trigger the ranging exchange.
 *     - a response message sent by the responder allowing the initiator to
 *       go on with the process.
 *     - a final message sent by the initiator to complete the exchange and
 *       provide all information needed by the responder to compute the
 *       time-of-flight (distance) estimate.
 *    The first 10 bytes of those frame are common and are composed of the
 *    following fields:
 *     - byte 0/1: frame control (0x8841 to indicate a data frame using
 *                 16-bit addressing).
 *     - byte 2:   sequence number, incremented for each new frame.
 *     - byte 3/4: PAN ID (0xDECA).
 *     - byte 5/6: destination address, see NOTE 3 below.
 *     - byte 7/8: source address, see NOTE 3 below.
 *     - byte 9:   function code (specific values to indicate which
 *                 message it is in the ranging process).
 *    The remaining bytes are specific to each message as follows:
 *    Poll message:
 *     - no more data
 *    Response message:
 *     - byte 10: activity code (0x02 to tell the initiator to go on with the
 *                ranging exchange).
 *     - byte 11/12: activity parameter, not used for activity code 0x02.
 *    Final message:
 *     - byte 10 -> 13: poll message transmission timestamp.
 *     - byte 14 -> 17: response message reception timestamp.
 *     - byte 18 -> 21: final message transmission timestamp.
 *    All messages end with a 2-byte checksum automatically set by DW1000.
 * 3. Source and destination addresses are hard coded constants in this example
 *    to keep it simple but for a real product every device should have a
 *    unique ID. Here, 16-bit addressing is used to keep the messages as short
 *    as possible but, in an actual application, this should be done only
 *    after an exchange of specific messages used to define those short
 *    addresses for each device participating to the ranging exchange.
 * 4. Delays between frames have been chosen here to ensure proper
 *    synchronisation of transmission and reception of the frames between
 *    the initiator and the responder and to ensure a correct accuracy of the
 *    computed distance. The user is referred to DecaRanging ARM Source Code
 *    Guide for more details about the timings involved in the ranging process.
 * 5. This timeout is for complete reception of a frame, i.e. timeout duration
 *    must take into account the length of the expected frame. Here the value
 *    is arbitrary but chosen large enough to make sure that there is enough
 *    time to receive the complete final frame sent by the responder at the
 *    110k data rate used (around 3.5 ms).
 * 6. The preamble timeout allows the receiver to stop listening in situations
 *    where preamble is not starting (which might be because the responder is
 *    out of range or did not receive the message to respond to). This saves
 *    the power waste of listening for a message that is not coming. We
 *    recommend a minimum preamble timeout of 5 PACs for short range
 *    applications and a larger value (e.g. in the range of 50% to 80% of the
 *    preamble length) for more challenging longer range, NLOS or noisy
 *    environments.
 * 7. In a real application, for optimum performance within regulatory limits,
 it may be necessary to set TX pulse bandwidth and TX power, (using
 *    the dwt_configuretxrf API call) to per device calibrated values saved
 *    in the target system or the DW1000 OTP memory.
 * 8. We use polled mode of operation here to keep the example as simple as
 *    possible but all status events can be used to generate interrupts. Please
 *    refer to DW1000 User Manual for more details on "interrupts". It is
 *    also to be noted that STATUS register is 5 bytes long but, as the event
 *    we use are all in the first bytes of the register, we can use the simple
 *    dwt_read32bitreg() API call to access it instead of reading the whole
 *    5 bytes.
 * 9. Timestamps and delayed transmission time are both expressed in device
 *    time units so we just have to add the desired response delay to poll RX
 *    timestamp to get response transmission time. The delayed transmission
 *    time resolution is 512 device time units which means that the lower 9 bits
 *    of the obtained value must be zeroed. This also allows to encode the
 *    40-bit value in a 32-bit words by shifting the all-zero lower 8 bits.
 * 10. dwt_writetxdata() takes the full size of the message as a parameter but
 *     only copies (size - 2) bytes as the check-sum at the end of the frame
 *     is automatically appended by the DW1000. This means that our variable
 *     could be two bytes shorter without losing any data (but the sizeof
 *     would not work anymore then as we would still have to indicate the full
 *     length of the frame to dwt_writetxdata()).
 * 11. When running this example on the EVB1000 platform with the
 *     POLL_RX_TO_RESP_TX_DLY response delay provided, the dwt_starttx() is
 *     always successful. However, in cases where the delay is too short (or
 *     something else interrupts the code flow), then the dwt_starttx() might
 *     be issued too late for the configured start time. The code below
 *     provides an example of how to handle this condition: In this case it
 *     abandons the ranging exchange and simply goes back to awaiting another
 *     poll message. If this error handling code was not here, a late
 *     dwt_starttx() would result in the code flow getting stuck waiting
 *     subsequent RX event that will will never come. The companion "initiator"
 *     example (ex_05a) should timeout from awaiting the "response" and proceed
 *     to send another poll in due course to initiate another ranging exchange.
 * 12. The high order byte of each 40-bit time-stamps is discarded here.
 *     This is acceptable as, on each device, those time-stamps are not
 *     separated by more than 2**32 device time units (which is around 67 ms)
 *     which means that the calculation of the round-trip delays can be
 *     handled by a 32-bit subtraction.
 * 13. The user is referred to DecaRanging ARM application (distributed with
 *     EVK1000 product) for additional practical example of usage, and to the
 *     DW1000 API Guide for more details on the DW1000 driver functions.
 ****************************************************************************/
