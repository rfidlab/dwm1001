double sd_init_mode(uint16 *otherTagAddress) {
    /* Write frame data to DW1000 and prepare transmission.
     * See NOTE 8 below.
     */
    double distance;
    //double tof;
    //double Ra;
    //double Da;
    int poll_frame_nb;
    int ret;
    uint16 currentInitID;
    uint16 currentRespID;
    uint16 srcAddress;
    uint16 destAddress;
    int packet_poll_request_nb;
    int currentMode=INIT_MODE;
    int packetKind;

    currentInitID=myID;
    currentRespID=0;
    printk("1. currentInitID: %02x; currentRespID: %02x\n", currentInitID, currentRespID);
    //int64 tof_dtu;
    /*LDN: the following line should be replaced with a function call that sets all
     * the relevant information in tha packet, instead of using a prewritten buffer
     * and just setting the frame sequence number
     */
    tx_poll_msg[ALL_MSG_SN_IDX] = frame_seq_nb;
    msg_set_src_address(tx_poll_msg, myID);
    msg_set_dest_address(tx_poll_msg, 65535);
    //printk("%s\n",tx_poll_msg);
    /* Zero offset in TX buffer. */
    dwt_writetxdata(sizeof(tx_poll_msg), tx_poll_msg, 0);

    /* Zero offset in TX buffer, ranging. */
    dwt_writetxfctrl(sizeof(tx_poll_msg), 0, 1);

    /* Start transmission, indicating that a response is expected so that
     * reception is enabled automatically after the frame is sent and the
     * delay set by dwt_setrxaftertxdelay() has elapsed.
     */
    //print_reg(SYS_STATUS_ID);
    /* Clear good RX frame event and TX frame sent in the DW1000
     * status register.
     */
    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_ALL_TX);
    //print_reg(SYS_STATUS_ID);
    //print_reg(TX_FCTRL_ID);
    ret=dwt_starttx(DWT_START_TX_IMMEDIATE | DWT_RESPONSE_EXPECTED);

    /* LDN: from here code added to replace register polling with interrupt polling*/
    //     printk("Process ISR  before starttx = %u\n",process_isr);
    //     dwt_setrxtimeout(0);
    //     dwt_starttx(DWT_START_TX_IMMEDIATE | DWT_RESPONSE_EXPECTED);
    //     printk("POLL packet (%u) sent\n",frame_seq_nb);
    //     while (process_isr != 1) {
    //         // Wait for interrupt because of TX event to happen.
    //     }
    //     printk("Process ISR after first while = %u\n",process_isr);
    //     /* Run dwt_isr */
    //     /* Note: this is out of the real ISR on purpose because zephyr
    //      * doesn't like APIs like spi being used during the ISR
    //      */
    //     dwt_isr();
    //
    /* reset variable to 0 */
    //     process_isr = 0;
    //      printk("Process ISR before second while = %u\n",process_isr);
    //    while (process_isr != 1) {
    //        // Wait for interrupt because of RX event to happen.
    //    }
    //         printk("Process ISR after second while = %u\n",process_isr);
    //    /* Run dwt_isr */
    //    /* Note: this is out of the real ISR on purpose because zephyr
    //     * doesn't like APIs like spi being used during the ISR
    //     */
    //    dwt_isr();

    /* reset variable to 0 */
    //    process_isr = 0;
    /* Until here: code to to replace register polling with interrupt polling*/

    /*LDN: Code to be commented out, if replaced by interrupt polling */
    if (ret == DWT_SUCCESS) {
        //printk("POLL packet sent\n");
        /* Poll DW1000 until TX frame sent event set.
         * See NOTE 9 below.
         */
        //print_reg(SYS_STATUS_ID);
        //print_reg(TX_FCTRL_ID);
        //int txlen=dwt_read32bitreg(SYS_STATUS_ID) & TX_FCTRL_TFLEN_MASK;
        //printk("TXLEN: %u\n",txlen);
        while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
        {};

        /* Clear TXFRS event. */
        dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);

        //printk("success (%u)\n", frame_seq_nb);

        /* Increment frame sequence number after transmission of the poll
         * message (modulo 256).
         */

        poll_frame_nb=frame_seq_nb;
        frame_seq_nb++;

        //return(distance);
    }
    else {
        printk("POLL message sending error - tx failed: %08lx\n", status_reg);
        return(-1);
    }

    /* We assume that the transmission is achieved correctly, poll for
     * reception of a frame or error/timeout. See NOTE 9 below.
     */
    while (!((status_reg = dwt_read32bitreg(SYS_STATUS_ID)) &
             (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_TO | SYS_STATUS_ALL_RX_ERR)))
    { };
    /* Code to be commented out, if replaced by interrupt polling until here */



    if (status_reg & SYS_STATUS_RXFCG) {
        uint32 frame_len;

        /* Clear good RX frame event and TX frame sent in the DW1000
         * status register.
         */
        dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_RXFCG | SYS_STATUS_TXFRS);

        /* A frame has been received, read it into the local buffer. */
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFLEN_MASK;
        if (frame_len <= RX_BUF_LEN) {
            dwt_readrxdata(rx_buffer, frame_len, 0);
        }

        /* Check that the frame is the expected response from the companion
         * "DS TWR responder" example.
         * As the sequence number field of the frame is not relevant,
         * it is cleared to simplify the validation of the frame.
         */
        recPacketProcess(rx_buffer, &packetKind, &srcAddress, &destAddress, &packet_poll_request_nb);
        int checkResult=recPacketCheck(currentMode, currentInitID, currentRespID, poll_frame_nb, packetKind, srcAddress, destAddress, packet_poll_request_nb);
        printk("2. currentInitID: %02x; currentRespID: %02x\n", currentInitID, currentRespID);
        printk("3. srcAddress: %02x; destAddress: %02x\n", srcAddress, destAddress);
        rx_buffer[ALL_MSG_SN_IDX] = 0;
        if(checkResult) {

            currentRespID=srcAddress;
            printk("4. srcAddress: %02x; currentRespID: %02x\n", srcAddress, currentRespID);
            //printk("Response packet received\n")
            uint32 final_tx_time;
            //          uint32 poll_rx_ts_32, resp_tx_ts_32, poll_tx_ts_32, resp_rx_ts_32;
            //int ret;
            /* Retrieve poll transmission and response reception timestamp. */
            poll_tx_ts = get_tx_timestamp_u64();
            resp_rx_ts = get_rx_timestamp_u64();

            /*LDN: this part was added to enable the INIT node to perform its own
             distance estimation, but mostly for debugging reasons: this estimate is not
             used, since the estimate sent by the RESP node will be way more accurate.
             Note that in order to enable this code one has to increase the length of
             RESPONSE frame of at least 8 bytes to make room for the time stamps, and
             uncomment the response_msg_get_ts() response_msg_set_ts() functions and the
             support uint variables commented a few lines above this.*/

            /* Get timestamps embedded in the response message. */
            //            response_msg_get_ts(&rx_buffer[RESPONSE_MSG_POLL_RX_TS_IDX], &poll_rx_ts_32);
            //             response_msg_get_ts(&rx_buffer[RESPONSE_MSG_RESP_TX_TS_IDX], &resp_tx_ts_32);
            //
            //             /* Compute time of flight. 32-bit subtractions give
            //              * correct answers even if clock has wrapped.
            //              * See NOTE 12 below.
            //              */
            //             poll_tx_ts_32 = (uint32)poll_tx_ts;
            //             resp_rx_ts_32 = (uint32)resp_rx_ts;
            //
            //             Ra = (double)(resp_rx_ts_32 - poll_tx_ts_32);
            //             Da = (double)(resp_tx_ts_32 - poll_rx_ts_32);
            //
            //             sprintf(dist_str, "Ra: %f, Da: %f\n",
            //                     Ra, Da);
            //             printk("%s", dist_str);
            //
            //             tof_dtu = (int64)((Ra  - Da) / 2);
            //
            //             tof = tof_dtu * DWT_TIME_UNITS;
            //             distance = tof * SPEED_OF_LIGHT;
            //             tof_static=tof;
            //             distance_static=distance;
            //             /* Display computed distance on console. */
            //             sprintf(dist_str, "dist (%u): %3.2f m\n",
            //                     frame_seq_nb_rx, (float)(distance));
            //             printk("%s", dist_str);
            /*LDNCHECK: until here*/



            /* Compute final message transmission time. See NOTE 10 below. */
            final_tx_time = (resp_rx_ts +
                             (RESP_RX_TO_FINAL_TX_DLY_UUS * UUS_TO_DWT_TIME)) >> 8;

            dwt_setdelayedtrxtime(final_tx_time);

            /* Final TX timestamp is the transmission time we programmed
             * plus the TX antenna delay.
             */
            final_tx_ts = (((uint64)(final_tx_time & 0xFFFFFFFEUL)) << 8) +
            TX_ANT_DLY;

            /* Write all timestamps in the final message.
             * See NOTE 11 below.
             */
            final_msg_set_ts(&tx_final_msg[FINAL_MSG_POLL_TX_TS_IDX], poll_tx_ts);
            final_msg_set_ts(&tx_final_msg[FINAL_MSG_RESP_RX_TS_IDX], resp_rx_ts);
            final_msg_set_ts(&tx_final_msg[FINAL_MSG_FINAL_TX_TS_IDX], final_tx_ts);

            /* Write and send final message.
             * See NOTE 8 below.
             */
            tx_final_msg[ALL_MSG_SN_IDX] = poll_frame_nb;
            msg_set_src_address(tx_final_msg, myID);
            msg_set_dest_address(tx_final_msg, currentRespID);
            /* Zero offset in TX buffer. */
            dwt_writetxdata(sizeof(tx_final_msg), tx_final_msg, 0);

            /* Zero offset in TX buffer, ranging. */
            dwt_writetxfctrl(sizeof(tx_final_msg), 0, 1);

            /*LDN: added here code to wait for confirmation packet RESP->INIT,
             including the estimated distance to be extracted and returned. We do this for at least 2 reasons:
             1) Distance estimation at the RESP node will be more accurate, as it is based on a 3-packets
             exchange (see DWM1000 User Manual);
             2) We want the same distance to be shared by both nodes, in order to avoid inconsistencies in
             determining violations of minimum safeety distances;
             Note that we could perform an additional distance estimation by asking the RESP to provide the required TimeStamps,
             but this is not done in order to avoid inconsistencies (see point 2) above) and to save time, because in this way the
             RESP node can send the confirmation packet immediately. */

            if(confirmationMessageOn)
            {
                ret = dwt_starttx(DWT_START_TX_DELAYED | DWT_RESPONSE_EXPECTED);
                /*LDN: the block of code below is for debugging, as transmission seems to hang here when DWT_RESPONSE_EXPECTED is used*/
                if (ret == DWT_SUCCESS) {
                    //printk("Final packet sent\n");
                    /* Poll DW1000 until TX frame sent event set.
                     * See NOTE 9 below.
                     */
                    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                    { };

                    /* Clear TXFRS event. */
                    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);

                    //printk("success (%u)\n", frame_seq_nb);

                    /* Increment frame sequence number after transmission of
                     * the final message (modulo 256).
                     */
                    frame_seq_nb++;

                    //return(distance);
                }
                else {
                    printk("FINAL message sending error - tx failed: %08lx\n", status_reg);
                    return(-1);
                }
                /*until here*/

                /* We assume that the transmission is achieved correctly, poll for
                 * reception of a frame or error/timeout. See NOTE 9 below.
                 */
                while (!((status_reg = dwt_read32bitreg(SYS_STATUS_ID)) &
                         (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_TO | SYS_STATUS_ALL_RX_ERR)))
                { };
                //printk("Out of the while\n");
                /* Increment frame sequence number after transmission of the poll
                 * message (modulo 256).
                 */
                //frame_seq_nb++;

                if (status_reg & SYS_STATUS_RXFCG) {
                    uint32 frame_len;
                    uint32 dist_int_received;
                    float distance_received;
                    /* Clear good RX frame event and TX frame sent in the DW1000
                     * status register.
                     */
                    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_RXFCG | SYS_STATUS_TXFRS);

                    /* A frame has been received, read it into the local buffer. */
                    frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFLEN_MASK;
                    if (frame_len <= RX_BUF_LEN) {
                        dwt_readrxdata(rx_buffer, frame_len, 0);
                    }

                    /* Check that the frame is the expected CONFIRM packet from the companion
                     * "DS TWR responder" example.
                     * As the sequence number field of the frame is not relevant,
                     * it is cleared to simplify the validation of the frame.
                     */
                    recPacketProcess(rx_buffer, &packetKind, &srcAddress, &destAddress, &packet_poll_request_nb);
                    int checkResult=recPacketCheck(currentMode, currentInitID, currentRespID, poll_frame_nb, packetKind, srcAddress, destAddress, packet_poll_request_nb);
                    printk("5. currentInitID: %02x; currentRespID: %02x\n", currentInitID, currentRespID);
                    printk("6. srcAddress: %02x; destAddress: %02x\n", srcAddress, destAddress);
                    rx_buffer[ALL_MSG_SN_IDX] = 0;
                    if(checkResult) {
                        conf_msg_get_distance(&rx_buffer[CONF_MSG_DIST_IDX], &dist_int_received);
                        distance_received=((double) dist_int_received)/1000;
                        //printk("CONFIRM packet received\n");
                        //printk("Success (%u)\n", poll_frame_nb);
                        // if(checkResult)
                        //                                 printk("Check ok\n");
                        //                         else
                        //                             printk("Check not ok\n");
                        return(distance_received);
                    }
                    else
                    {
                        //printk("Error receiving CONFIRM packet\n");
                        return(-1);
                    }
                }
                else {
                    printk("Timeout/failure while waiting for CONFIRM packet\n");
                    return(-1);
                }
            }
            else
            {
                ret = dwt_starttx(DWT_START_TX_DELAYED);
                //return(distance);

                /* If dwt_starttx() returns an error, abandon this ranging
                 * exchange and proceed to the next one. See NOTE 12 below.
                 */

                if (ret == DWT_SUCCESS) {
                    /* Poll DW1000 until TX frame sent event set.
                     * See NOTE 9 below.
                     */
                    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                    { };

                    /* Clear TXFRS event. */
                    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);

                    //printk("Success (%u)\n", poll_frame_nb);

                    /* Increment frame sequence number after transmission of
                     * the final message (modulo 256).
                     */
                    frame_seq_nb++;
                    *otherTagAddress=currentRespID;
                    printk("7. otherTagAddress: %02x; currentRespID: %02x\n", *otherTagAddress, currentRespID);
                    return(distance);
                }
                else {
                    printk("FINAL message sending error - tx failed: %08lx\n", status_reg);
                    return(-1);
                }
            }
        }
        else {
            printk("Received wrong packet while waiting for a RESPONSE packet.\n");
            return(-1);
        }
    }
    else {
        printk("Timeout/failure while waiting for RESPONSE packet\n");

        /* Clear RX error/timeout events in the DW1000 status register. */
        dwt_write32bitreg(SYS_STATUS_ID,
                          SYS_STATUS_ALL_RX_TO | SYS_STATUS_ALL_RX_ERR);

        /* Reset RX to properly reinitialise LDE operation. */
        dwt_rxreset();
        return(0);
    }


    return(0);
}
