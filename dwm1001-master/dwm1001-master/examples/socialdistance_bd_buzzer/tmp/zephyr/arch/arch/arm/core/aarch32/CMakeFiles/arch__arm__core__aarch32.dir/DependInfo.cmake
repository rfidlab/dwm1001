# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/cpu_idle.S" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/cpu_idle.S.obj"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/exc_exit.S" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/exc_exit.S.obj"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/fault_s.S" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/fault_s.S.obj"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/isr_wrapper.S" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/isr_wrapper.S.obj"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/nmi_on_reset.S" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/nmi_on_reset.S.obj"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/swap_helper.S" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/swap_helper.S.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__PROGRAM_START"
  "__ZEPHYR_SUPERVISOR__"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "/Users/lucadn/zephyrproject/zephyr/kernel/include"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/include"
  "/Users/lucadn/zephyrproject/zephyr/include"
  "zephyr/include/generated"
  "/Users/lucadn/zephyrproject/zephyr/soc/arm/nordic_nrf/nrf52"
  "/Users/lucadn/zephyrproject/zephyr/ext/lib/crypto/tinycrypt/include"
  "/Users/lucadn/zephyrproject/zephyr/ext/hal/cmsis/Core/Include"
  "/Users/lucadn/zephyrproject/zephyr/subsys/bluetooth"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/nrfx"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/nrfx/drivers/include"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/nrfx/mdk"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/."
  "/Users/lucadn/zephyrproject/modules/debug/segger/rtt"
  "/Users/lucadn/zephyrproject/zephyr/lib/libc/minimal/include"
  "/Applications/gcc-arm-none-eabi-9-2019-q4-major/bin/../lib/gcc/arm-none-eabi/9.2.1/include"
  "/Applications/gcc-arm-none-eabi-9-2019-q4-major/bin/../lib/gcc/arm-none-eabi/9.2.1/include-fixed"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/fatal.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/fatal.c.obj"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/irq_manage.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/irq_manage.c.obj"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/nmi.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/nmi.c.obj"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/prep_c.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/prep_c.c.obj"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/swap.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/swap.c.obj"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/core/aarch32/thread.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/zephyr/arch/arch/arm/core/aarch32/CMakeFiles/arch__arm__core__aarch32.dir/thread.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__PROGRAM_START"
  "__ZEPHYR_SUPERVISOR__"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/lucadn/zephyrproject/zephyr/kernel/include"
  "/Users/lucadn/zephyrproject/zephyr/arch/arm/include"
  "/Users/lucadn/zephyrproject/zephyr/include"
  "zephyr/include/generated"
  "/Users/lucadn/zephyrproject/zephyr/soc/arm/nordic_nrf/nrf52"
  "/Users/lucadn/zephyrproject/zephyr/ext/lib/crypto/tinycrypt/include"
  "/Users/lucadn/zephyrproject/zephyr/ext/hal/cmsis/Core/Include"
  "/Users/lucadn/zephyrproject/zephyr/subsys/bluetooth"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/nrfx"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/nrfx/drivers/include"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/nrfx/mdk"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/."
  "/Users/lucadn/zephyrproject/modules/debug/segger/rtt"
  "/Users/lucadn/zephyrproject/zephyr/lib/libc/minimal/include"
  "/Applications/gcc-arm-none-eabi-9-2019-q4-major/bin/../lib/gcc/arm-none-eabi/9.2.1/include"
  "/Applications/gcc-arm-none-eabi-9-2019-q4-major/bin/../lib/gcc/arm-none-eabi/9.2.1/include-fixed"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
